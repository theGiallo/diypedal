#if 0
	USER=pi
	REMOTE=192.168.1.3
	//REMOTE=rpi00
	REMOTE_PATH=/home/pi/
	TMUX_TARGET=0
	REL_ROOT_PROJ=../

	THIS_FILE_PATH=$0
	THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	#THIS_FILE_DIR=${THIS_FILE_PATH%/*}
	THIS_FILE=${THIS_FILE_PATH##*/}
	EXECUTABLE_NAME=${THIS_FILE%%.*}

	#echo this file dir = "'$THIS_FILE_DIR'"
	#echo this file path = "'$THIS_FILE_PATH'"
	#echo this file = "'$THIS_FILE'"
	#echo executable name = "'$EXECUTABLE_NAME'"

	if [ -z "${CXX+x}" ]
	then
		CXX=arm-linux-gnueabihf-g++
	fi

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/include/"

	declare -a LIBS_DIRS
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/lib/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	#LIBS[${#LIBS[@]}]=pigpiod_if2
	LIBS[${#LIBS[@]}]=pigpio
	LIBS[${#LIBS[@]}]=rt

	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	$CXX -std=c++11 -Wall -Wextra -Wno-comment `include_dirs` `libs_dirs` `link_libs` -O3 -fPIC -o "$EXECUTABLE_NAME" "$THIS_FILE";
	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		printf 'will copy file to remote and run it\n'
		printf "user = '$USER'\n"
		printf "remote = '$REMOTE'\n"

		scp -i ../bin/pi_id_rsa "./$EXECUTABLE_NAME" $USER@$REMOTE:$REMOTE_PATH \
		&& \
		ssh -i ../bin/pi_id_rsa $USER@$REMOTE "tmux send-keys -t $TMUX_TARGET sudo\ $REMOTE_PATH$EXECUTABLE_NAME \"$@\" Enter"

		ret=$?

		if [ ! $ret -eq 0 ]
		then
			printf "scp failed\n"
		fi
		trash "$EXECUTABLE_NAME"
		#trash "$EXECUTABLE_NAME.o"
	else
		printf 'compilation failed\n' >&2;
	fi


	echo "exiting $ret"
	exit $ret
#endif
#define MCP4822_SPI_TEST 1
#include "mcp4822.h"

int
main( int argc, char *argv[] )
{
	(void)( argc );
	(void)( argv );

	int ret = 0;
	ret = mcp4822_test( argc, argv );
	return ret;
}
#include "spi.cpp"
#include "mcp4822.cpp"
#include "tggpio.cpp"
