#ifndef __TGTIME_H__
#define __TGTIME_H__ 1

#include <time.h>
//#include <stdlib.h>
#include "basic_types.h"

#ifndef CPU_CLOCK_FREQ
#define CPU_CLOCK_FREQ 700000000
#warning "using default value for CPU_CLOCK_FREQ"
#endif
#define CCNT_DELTA( s, e ) ((e) >= (s) ? (e)-(s) : (0xffffffff)-(s)+(e) )
#if 0
#define CPU_CLOCK_FROM_NS(ns) ((ns)*f32((CPU_CLOCK_FREQ)/(1e9f)))
#define NS_FROM_CPU_CLOCK(ck) ((ck)*f32(1e9f/f32(CPU_CLOCK_FREQ)))
#else
#define CPU_CLOCK_FROM_NS(ns) ((ns)*f64(f64(CPU_CLOCK_FREQ)/(1e9)))
#define NS_FROM_CPU_CLOCK(ck) ((ck)*f64(1e9/f64(CPU_CLOCK_FREQ)))
#endif

#if ARM8
static inline uint32_t ccnt_read (void)
{
	uint32_t cc = 0;
	__asm__ volatile ("mrc p15, 0, %0, c9, c13, 0":"=r" (cc));
	return cc;
}
#endif
#define ARM7
#ifdef ARM7
static inline unsigned ccnt_read (void)
{
	unsigned cc;
	asm volatile ("mrc p15, 0, %0, c15, c12, 1" : "=r" (cc));
	return cc;
}
static inline void ccnt_init()
{
	#define CCNT_E        (1<<0)  // enable all three counters
	#define CCNT_P        (1<<1)  // reset two count registers
	#define CCNT_C        (1<<2)  // reset cycle counter register
	#define CCNT_D        (1<<3)  // cycle count divider (64)
	#define CCNT_PMNC(v)  asm volatile("mcr p15, 0, %0, c15, c12, 0" :: "r"(v))
	#define CCNT_CCNT(v)  asm volatile("mrc p15, 0, %0, c15, c12, 1" : "=r"(v))
	CCNT_PMNC(CCNT_E|CCNT_P);
}
#endif
#if THIS_IS_THE_KERNEL_MODULE_FOR_ARM8
void enable_ccr(void *info)
{
  // Set the User Enable register, bit 0
  asm volatile ("mcr p15, 0, %0, c9, c14, 0" :: "r" (1));
  // Enable all counters in the PNMC control-register
  asm volatile ("MCR p15, 0, %0, c9, c12, 0\t\n" :: "r"(1));
  // Enable cycle counter specifically
  // bit 31: enable cycle counter
  // bits 0-3: enable performance counters 0-3
  asm volatile ("MCR p15, 0, %0, c9, c12, 1\t\n" :: "r"(0x80000000));
}
#endif

#define NANO_NOW_NS_TAKEN 6000

inline
u64
nano_now()
{
	struct timespec t;
	int ret;
	ret = clock_gettime( CLOCK_MONOTONIC, &t );
	if ( ret != 0 )
	{
		log_err( "clock_gettime failed" );
		//exit( 0 );
		return 0;
	}
	return u64(t.tv_sec) * u64(1e9) + u64(t.tv_nsec);
}
struct
cksleep_busy_up_to_get_return
{
	u32 now_ck;
	u32 sleeped_ck;
};
inline
cksleep_busy_up_to_get_return
cksleep_busy_up_to_get( u32 start_ck, u32 tot_ck )
{
	cksleep_busy_up_to_get_return ret;

	u32 now_ck, sleep_start_ck, target_ck;

	CCNT_CCNT( now_ck );
	u32 already_passed = CCNT_DELTA( start_ck, now_ck );
	sleep_start_ck = now_ck;
	if ( tot_ck > already_passed )
	{
		target_ck = now_ck + ( tot_ck - already_passed );
		if ( sleep_start_ck > target_ck )
		{
			u32 prev_ck = now_ck;
			do { prev_ck = now_ck; CCNT_CCNT( now_ck ); /*asm volatile("nop");*/ } while ( prev_ck < now_ck );
		}

		{
			u32 prev_ck = now_ck;
			do { prev_ck = now_ck; CCNT_CCNT( now_ck ); /*asm volatile("nop");*/ } while ( prev_ck < now_ck && now_ck < target_ck );
		}
	}

	ret.sleeped_ck = CCNT_DELTA( sleep_start_ck, now_ck );
	ret.now_ck = now_ck;

	return ret;
}
inline
void
cksleep_busy( u32 tot_ck )
{
	u32 now_ck, start_ck, target_ck;

	CCNT_CCNT( now_ck );
	start_ck = now_ck;
	target_ck = now_ck + tot_ck;

	if ( start_ck > target_ck )
	{
		u32 prev_ck = now_ck;
		do { prev_ck = now_ck; CCNT_CCNT( now_ck ); /*asm volatile("nop");*/ } while ( prev_ck < now_ck );
	}

	{
		u32 prev_ck = now_ck;
		do { prev_ck = now_ck; CCNT_CCNT( now_ck ); /*asm volatile("nop");*/ } while ( prev_ck < now_ck && now_ck < target_ck );
	}
}
struct
nsleep_busy_up_to_get_return
{
	u64 now_ns;
	u64 sleeped_ns;
};
inline
nsleep_busy_up_to_get_return
nsleep_busy_up_to_get( u64 start_ns, u64 tot_nsec )
{
	nsleep_busy_up_to_get_return ret;
	u64 now_ns, sleep_start_ns, target_ns;
	target_ns = start_ns + tot_nsec - NANO_NOW_NS_TAKEN;
	sleep_start_ns = now_ns = nano_now();
	while ( now_ns < target_ns ) { now_ns = nano_now(); }
	ret.sleeped_ns = now_ns < sleep_start_ns ? 0 : now_ns - sleep_start_ns;
	ret.now_ns = now_ns;
	return ret;
}
inline
void
nsleep_busy_up_to( u64 start_ns, u64 tot_nsec )
{
	u64 end_ns, target_ns;
	target_ns = start_ns + tot_nsec - NANO_NOW_NS_TAKEN;
	while ( ( end_ns = nano_now() ) < target_ns ) { }
}
inline
void
nsleep_busy( u64 nsec_sleep )
{
	u64 start_ns, end_ns, target_ns;
	start_ns = nano_now();
	target_ns = start_ns + nsec_sleep - NANO_NOW_NS_TAKEN;
	while ( ( end_ns = nano_now() ) < target_ns ) { }
}
inline
void
usleep_busy_up_to( u64 start_ns, u64 tot_usec )
{
	u64 end_ns, target_ns;
	target_ns = start_ns + tot_usec * 1000 - NANO_NOW_NS_TAKEN;
	while ( ( end_ns = nano_now() ) < target_ns ) { }
}
inline
void
usleep_busy( u64 usec_sleep )
{
	u64 start_ns, end_ns, target_ns;
	start_ns = nano_now();
	target_ns = start_ns + usec_sleep * 1000 - NANO_NOW_NS_TAKEN;
	while ( ( end_ns = nano_now() ) < target_ns ) { }
}

#endif /* ifndef __TGTIME_H__ */
