#if 0
	USER=pi
	REMOTE=192.168.1.4
	#REMOTE=rpi00
	REMOTE_PATH=/home/pi/
	TMUX_TARGET=0
	REL_ROOT_PROJ=../

	THIS_FILE_PATH=$0
	THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	#THIS_FILE_DIR=${THIS_FILE_PATH%/*}
	THIS_FILE=${THIS_FILE_PATH##*/}
	EXECUTABLE_NAME=${THIS_FILE%%.*}

	#echo this file dir = "'$THIS_FILE_DIR'"
	#echo this file path = "'$THIS_FILE_PATH'"
	#echo this file = "'$THIS_FILE'"
	#echo executable name = "'$EXECUTABLE_NAME'"

	if [ -z "${CXX+x}" ]
	then
		CXX=arm-linux-gnueabihf-g++
	fi

	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]='APP_NAME="diypedal"'
#DEFINES[${#DEFINES[@]}]='DEBUG=1'

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/include/"
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/repo/"

	declare -a LIBS_DIRS
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/lib/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	#LIBS[${#LIBS[@]}]=pigpiod_if2
	#LIBS[${#LIBS[@]}]=pigpio
	LIBS[${#LIBS[@]}]=rt

	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	$CXX -std=c++11 -fpermissive -Wall -Wextra -Wno-comment `defines` `include_dirs` `libs_dirs` `link_libs` -O3 -fPIC -o "$EXECUTABLE_NAME" "$THIS_FILE";
	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		printf 'will copy file to remote and run it\n'
		printf "user = '$USER'\n"
		printf "remote = '$REMOTE'\n"

		scp -i ../bin/pi_id_rsa "./$EXECUTABLE_NAME" $USER@$REMOTE:$REMOTE_PATH \
		&& \
		ssh -i ../bin/pi_id_rsa $USER@$REMOTE "tmux send-keys -t $TMUX_TARGET sudo\ $REMOTE_PATH$EXECUTABLE_NAME\ \"$@\" Enter"

		ret=$?

		if [ ! $ret -eq 0 ]
		then
			printf "scp failed\n"
		fi
		trash "$EXECUTABLE_NAME"
		#trash "$EXECUTABLE_NAME.o"
	else
		printf 'compilation failed\n' >&2;
	fi


	echo "exiting $ret"
	exit $ret
#endif
#include <sys/resource.h> // setpriority(...)
#include <errno.h> // perror()
#include <stdlib.h>
#include <math.h>
#include "tgsched.h"
#include "basic_types.h"
#include "macro_tools.h"
#include "tgtime.h"
#include "mcp4822.h"
#include "mcp3202.h"
#include "tggpio.h"
#include "loop_pedal.h"

#define DEBUG_BUTTONS 0

#define SLEEP_NO 0
#define SLEEP_CCNT 1
#define SLEEP_NANO_NOW 2

#define SLEEP SLEEP_NO
#define SLEEP_ONCE 1
#define PERF_GETTIME 0
#define PERF_CCNT 0
#define PERF_ADC_DAC 0
#define PERF_CYCLE 0
#define PERF_CYCLE_FULL 0
#define RENICE 1
#define NICE_VALUE -20
#define DO_SCHED_DEADLINE 1
#define DO_SCHED_FIFO 0
#define SCHED_YIELD 1
#define TRACE_VALUES 0
#define TARGET_Hz 44100
#define MAX_LOOP_SECONDS 60
#define RUN_FOR_SECS 60
int
main( int argc, char *argv[] )
{
	(void)( argc );
	(void)( argv );

	int ret = 0;

	u64 start_ns, end_ns, tot_ns, tot_ns_sleeped = 0, tot_ck_sleeped = 0;
	f64 ns_ps, Hz;
	//#if 1
	//u64 usec_sleep = 1e6f / f32(TARGET_Hz); // 22.6757
	//#else
	//u64 usec_sleep = 1000f;
	//#endif
	#if SLEEP == SLEEP_NANO_NOW
	u64 in_cycle_start_ns = 0;
	#if ! SLEEP_ONCE
	u64 out_cycle_start_ns = 0
	#endif
	#endif
	u64 nsec_per_cycle = f64(1e9) / f64(TARGET_Hz);
	u64 nsec_per_cycle_work = nsec_per_cycle * 0.5f;// 0.98f;

	#if SLEEP == SLEEP_CCNT
	u32 in_cycle_start_ck = 0;
	CCNT_CCNT( in_cycle_start_ck );
	#if ! SLEEP_ONCE
	u32 out_cycle_start_ck = 0;
	CCNT_CCNT( out_cycle_start_ck );
	#endif
	#endif
	const u32 CPU_FREQ = CPU_CLOCK_FREQ;
	const u32 ck_per_cycle = CPU_FREQ / f64(TARGET_Hz);
	const f32 ck_to_ns = ( f64(1e9) / f64(CPU_FREQ) );

	spi_init( SPI_Device::Lib::PIGPIO );
	tggpio_setup();


	#if DO_SCHED_DEADLINE
	{
		//int i_ret = tgsched_deadline_self( nsec_per_cycle, nsec_per_cycle_work );

		// NOTE(theGiallo): with this values we run at avg of 45.6kHz with 60~us of max cycle
		//int i_ret = tgsched_deadline_self( 2048*8, 1024 );

		// --- // NOTE(theGiallo): with this values we run at avg of 43.3kHz with 60~us of max cycle
		// NOTE(theGiallo): with this values we run at avg of 45.6kHz with 60~us of max cycle
		//int i_ret = tgsched_deadline_self( nsec_per_cycle >> 1, nsec_per_cycle_work >> 1 );

		u32 period  = nsec_per_cycle     ;// / 8;
		u32 runtime = nsec_per_cycle_work;// / 8;
		log_info( "runtime = %d period = %d %f", runtime, period, runtime / (f32)period );
		errno = 0;
		int i_ret = tgsched_deadline_self( period, runtime );

		//do
		//{
			//int i_ret = tgsched_deadline_self( nsec_per_cycle / 6, nsec_per_cycle_work / 6 );

			if ( i_ret < 0 )
			{
				log_err( "could not set scheduler to SCHED_DEADLINE err = %d errn = %d", i_ret, errno );
				perror( "tgsched_setattr" );
				ret = -1;
				//return ret;
				//goto cannot_set_schedule;
				//exit( -1 );
			}
		//} while ( errno == 16 && (sched_yield(),true) );
	}
	#elif DO_SCHED_FIFO
	{
		errno = 0;
		int i_ret = tgsched_fifo_self();
		if ( i_ret < 0 )
		{
			log_err( "could not set scheduler to SCHED_FIFO err = %d errn = %d", i_ret, errno );
			perror( "tgsched_fifo_self" );
			ret = -1;
		}
	}
	#endif

	{
		//enable_ccr(0);
		ccnt_init();
		//u32 ccnt_s = ccnt_read();
		u32 ccnt_s, ccnt_e;
		CCNT_CCNT(ccnt_s);
		u64 nn_s = nano_now();
		CCNT_CCNT(ccnt_e);
		//u32 ccnt_e = ccnt_read();
		u64 nn_e = nano_now();
		u32 ccnt_tot = ccnt_e - ccnt_s;
		fprintf( stderr, "double nano_now() took %u ticks\n", ccnt_tot );
		u64 nn = nn_e - nn_s;
		fprintf( stderr, "double nano_now() took %lluns\n", nn );
	}

	#if RENICE
	{
		int which = PRIO_PROCESS;
		id_t pid;
		int priority = NICE_VALUE;
		int ret;

		pid = getpid();
		ret = setpriority(which, pid, priority);
		if ( ret == -1 )
		{
			log_err( "failed to renice itself to %d", priority );
			perror( 0 );
		}
	}
	#endif

	#if PERF_GETTIME
	#if PERF_ADC_DAC
	const char * perf_adc_file_path      = "adc_times_ns.txt";
	const char * perf_dac_file_path      = "dac_times_ns.txt";
	#endif
	#if PERF_CYCLE
	const char * perf_cycle_file_path    = "cycle_times_ns.txt";
	#endif
	#if PERF_CYCLE_FULL
	const char * perf_cyclefull_file_path    = "cyclefull_times_ns.txt";
	#endif
	#endif
	#if PERF_CCNT
	#if PERF_ADC_DAC
	const char * perf_ck_adc_file_path   = "adc_ck_times_ns.txt";
	const char * perf_ck_dac_file_path   = "dac_ck_times_ns.txt";
	#endif
	#if PERF_CYCLE
	const char * perf_ck_cycle_file_path = "cycle_ck_times_ns.txt";
	#endif
	#if PERF_CYCLE_FULL
	const char * perf_ck_cyclefull_file_path = "cyclefull_ck_times_ns.txt";
	#endif
	#endif

	char * out_file_path = 0;
	if ( argc == 2 )
	{
		out_file_path = argv[1];
	}

	//const int OUT_PINS_COUNT = 2;
	const int VALUES_COUNT = TARGET_Hz * RUN_FOR_SECS;
	const int TOTAL_VALUES = VALUES_COUNT;
	const f32 VOLTAGE_MAX = MCP4822_SPI::OUT_MAX_V;
	#if TRACE_VALUES
	f32 * values = (f32*)calloc( sizeof ( f32 ), VALUES_COUNT );
	#endif
	#if PERF_GETTIME
	#if PERF_ADC_DAC
	u64 * ns_adc   = (u64*)calloc( sizeof ( u64 ), VALUES_COUNT );
	u64 * ns_dac   = (u64*)calloc( sizeof ( u64 ), VALUES_COUNT );
	#endif
	#if PERF_CYCLE
	u64 * ns_cycle = (u64*)calloc( sizeof ( u64 ), VALUES_COUNT );
	#endif
	#if PERF_CYCLE_FULL
	u64 * ns_cycle_full = (u64*)calloc( sizeof ( u64 ), VALUES_COUNT );
	#endif
	#endif
	#if PERF_CCNT
	#if PERF_ADC_DAC
	u32 * ck_adc   = (u32*)calloc( sizeof ( u32 ), VALUES_COUNT );
	u32 * ck_dac   = (u32*)calloc( sizeof ( u32 ), VALUES_COUNT );
	#endif
	#if PERF_CYCLE
	u32 * ck_cycle = (u32*)calloc( sizeof ( u32 ), VALUES_COUNT );
	#endif
	#if PERF_CYCLE_FULL
	u32 * ck_cyclefull = (u32*)calloc( sizeof ( u32 ), VALUES_COUNT );
	#endif
	#endif

	Loop_Pedal loop_pedal = {};
	loop_pedal_init( &loop_pedal, TARGET_Hz * MAX_LOOP_SECONDS );

	tggpio_Button rec_button = {};
	tggpio_button_setup( &rec_button, 17 );
	tggpio_Button play_button = {};
	tggpio_button_setup( &play_button, 27 );
	tggpio_Button stop_button = {};
	tggpio_button_setup( &stop_button, 22 );

	const u32 buttons_cycles_period = 400;
	const u32 buttons_count = 3;
	const u32 buttons_cycles_period_step = buttons_cycles_period / buttons_count;
	u32 rec_button_cycles_counter  = 0 * buttons_cycles_period_step;
	u32 play_button_cycles_counter = 1 * buttons_cycles_period_step;
	u32 stop_button_cycles_counter = 2 * buttons_cycles_period_step;



	float   voltage   = 0;
	uint8_t mcp_index = 0;

	MCP3202_SPI mcp3202_dev;
	mcp3202_init( &mcp3202_dev );

	MCP4822_SPI mcp4822_dev;
	mcp4822_init( &mcp4822_dev, 0xFF );

	mcp3202_dev.spi_device.SPI_number    = 0;
	mcp3202_dev.spi_device.SPI_channel   = 0;

	// NOTE(theGiallo): freq max is 0.9MHz with VDD=2.7V and 1.8 with VDD=5V
	#if 1
	mcp3202_dev.spi_device.speed_hz      = 1800000;
	mcp3202_dev.spi_device.speed_hz      = 3600000;
	mcp3202_dev.spi_device.speed_hz      = 4000000;
	//mcp3202_dev.reference_voltage        = 5.0f;
	mcp3202_dev.reference_voltage        = 4.096f;
	//mcp3202_dev.reference_voltage        = 4.100f;
	#else
	mcp3202_dev.spi_device.speed_hz      = 900000;
	mcp3202_dev.reference_voltage        = 3.33f;
	#endif

	bool b_res;
	b_res = spi_open( &mcp3202_dev.spi_device );
	if ( !b_res )
	{
		ret = 1;
		goto mcp3202_exit_cannot_open;
	}

	mcp4822_dev.spi_device.SPI_number    = 0;
	mcp4822_dev.spi_device.SPI_channel   = 1;

	b_res = spi_open( &mcp4822_dev.spi_device );
	if ( !b_res )
	{
		ret = 1;
		goto mcp4822_exit_cannot_open;
	}


	voltage = 1.024f;
	mcp_index = 0;
	//out_cycle_start_ns = nano_now();
	b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, voltage );
	if ( ! b_res )
	{
		fprintf( stderr, "error outputting voltage from pin %u\n", (uint32_t)mcp_index );
		ret = 1;
		goto mcp4822_exit_close;
	}
	//nsleep_busy( 100 );

	#if SLEEP == SLEEP_CCNT
	CCNT_CCNT( in_cycle_start_ck );
	#endif
	#if SLEEP == SLEEP_NANO_NOW
	in_cycle_start_ns =
	#endif
	start_ns = nano_now();
	log_info( "starting program main loop" );
	for ( int i = 0; i != VALUES_COUNT; ++i )
	{
		#if PERF_GETTIME
		#if PERF_ADC_DAC
		u64 perf_start, perf_end;
		#endif
		#if PERF_CYCLE
		u64 perf_cycle_start, perf_cycle_end;
		perf_cycle_start = nano_now();
		#endif
		#if PERF_CYCLE_FULL
		u64 perf_cyclefull_start, perf_cyclefull_end;
		perf_cyclefull_start = nano_now();
		#endif
		#endif
		#if PERF_CCNT
		#if PERF_ADC_DAC
		u32 perf_ccnt_start, perf_ccnt_end;
		#endif
		#if PERF_CYCLE
		u32 perf_ccnt_cycle_start, perf_ccnt_cycle_end;
		CCNT_CCNT( perf_ccnt_cycle_start );
		#endif
		#if PERF_CYCLE_FULL
		u32 perf_ccnt_cyclefull_start, perf_ccnt_cyclefull_end;
		CCNT_CCNT( perf_ccnt_cyclefull_start );
		#endif
		#endif


		voltage   = 0;
		mcp_index = 0;

		#if !SLEEP_ONCE
		#if SLEEP == SLEEP_NANO_NOW
		{
			nsleep_busy_up_to_get_return sr = nsleep_busy_up_to_get( in_cycle_start_ns, nsec_per_cycle );
			tot_ns_sleeped += sr.sleeped_ns;
			in_cycle_start_ns = sr.now_ns;
		}
		#elif SLEEP == SLEEP_CCNT
		{
			cksleep_busy_up_to_get_return sr = cksleep_busy_up_to_get( in_cycle_start_ck, ck_per_cycle );
			tot_ck_sleeped += sr.sleeped_ck;
			in_cycle_start_ck = sr.now_ck;
		}
		#endif
		#endif

		#if PERF_ADC_DAC
		#if PERF_GETTIME
		perf_start = nano_now();
		#endif
		#if PERF_CCNT
		CCNT_CCNT( perf_ccnt_start );
		#endif
		#endif

		b_res = mcp3202_spi_read_value( &mcp3202_dev, mcp_index, &voltage );

		#if PERF_ADC_DAC
		#if PERF_CCNT
		CCNT_CCNT( perf_ccnt_end );
		ck_adc[i] = CCNT_DELTA( perf_ccnt_start, perf_ccnt_end );// perf_ccnt_end - perf_ccnt_start;
		#endif
		#if PERF_GETTIME
		perf_end = nano_now();
		ns_adc[i] = perf_end - perf_start;
		#endif
		#endif

		if ( ! b_res )
		{
			fprintf( stderr, "error reading voltage from pin %u\n", (uint32_t)mcp_index );
			ret = 1;
			goto mcp3202_exit_close;
		}
		#if TRACE_VALUES
		else
		{
			values[i] = voltage;
		}
		#endif

		if ( ++rec_button_cycles_counter == buttons_cycles_period )
		{
			rec_button_cycles_counter = 0;
			tggpio_button_read( &rec_button );
			#if DEBUG_BUTTONS
			if ( rec_button.front != tggpio_Button::Front::STATIC )
			{
				log_info( "[%d]rec button: %d", i, rec_button.front );
			}
			#endif
			if ( rec_button.front == tggpio_Button::Front::DOWN )
			{
				loop_pedal.is_recording = ! loop_pedal.is_recording;
				loop_pedal.is_playing   = ! loop_pedal.is_recording;
				if ( loop_pedal.is_playing )
				{
					loop_pedal.play_index = 0;
				}
				if ( loop_pedal.is_recording )
				{
					loop_pedal.record_index = 0;
					loop_pedal.record_count = 0;
				}
				#if DEBUG_BUTTONS
				log_info( "[%d] recording  = %d", i, loop_pedal.is_recording );
				log_info( "[%d] is_playing = %d", i, loop_pedal.is_playing );
				#endif
			}
		}
		if ( ++play_button_cycles_counter == buttons_cycles_period )
		{
			play_button_cycles_counter = 0;
			if ( ! loop_pedal.is_recording )
			{
				tggpio_button_read( &play_button );
				#if DEBUG_BUTTONS
				if ( play_button.front != tggpio_Button::Front::STATIC )
				{
					log_info( "[%d]play button: %d", i, play_button.front );
				}
				#endif
				if ( play_button.front == tggpio_Button::Front::DOWN )
				{
					loop_pedal.is_playing = ! loop_pedal.is_playing;
					#if DEBUG_BUTTONS
					log_info( "[%d] recording  = %d", i, loop_pedal.is_recording );
					log_info( "[%d] is_playing = %d", i, loop_pedal.is_playing );
					#endif
					//if ( loop_pedal.is_playing )
					//{
					//    loop_pedal.play_index = 0;
					//}
				}
			}
		}
		if ( ++stop_button_cycles_counter == buttons_cycles_period )
		{
			stop_button_cycles_counter = 0;
			if ( loop_pedal.is_playing )
			{
				tggpio_button_read( &stop_button );

				//log_info( "[%d]stop button status   : %d", i, stop_button.status );
				#if DEBUG_BUTTONS
				if ( stop_button.front != tggpio_Button::Front::STATIC )
				{
					log_info( "[%d]stop button: %d", i, stop_button.front );
				}
				#endif
				if ( stop_button.front == tggpio_Button::Front::DOWN )
				{
					loop_pedal.is_playing = 0;
					loop_pedal.play_index = 0;
					#if DEBUG_BUTTONS
					log_info( "[%d] recording  = %d", i, loop_pedal.is_recording );
					log_info( "[%d] is_playing = %d", i, loop_pedal.is_playing );
					#endif
				}
			}
		}

		loop_pedal_cycle( &loop_pedal, voltage, &voltage );

		#if !SLEEP_ONCE
		#if SLEEP == SLEEP_NANO_NOW
		{
			nsleep_busy_up_to_get_return sr = nsleep_busy_up_to_get( out_cycle_start_ns, nsec_per_cycle );
			tot_ns_sleeped += sr.sleeped_ns;
			out_cycle_start_ns = sr.now_ns;
		}
		#elif SLEEP == SLEEP_CCNT
		{
			cksleep_busy_up_to_get_return sr = cksleep_busy_up_to_get( out_cycle_start_ck, ck_per_cycle );
			tot_ck_sleeped += sr.sleeped_ck;
			out_cycle_start_ck = sr.now_ck;
		}
		#endif
		#endif

		#if PERF_ADC_DAC
		#if PERF_GETTIME
		perf_start = nano_now();
		#endif
		#if PERF_CCNT
		CCNT_CCNT( perf_ccnt_start );
		#endif
		#endif

		b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, voltage );

		#if PERF_ADC_DAC
		#if PERF_CCNT
		CCNT_CCNT( perf_ccnt_end );
		ck_dac[i] = CCNT_DELTA( perf_ccnt_start, perf_ccnt_end );// perf_ccnt_end - perf_ccnt_start;
		#endif
		#if PERF_GETTIME
		perf_end = nano_now();
		ns_dac[i] = perf_end - perf_start;
		#endif
		#endif

		if ( ! b_res )
		{
			fprintf( stderr, "error outputting voltage from pin %u\n", (uint32_t)mcp_index );
			ret = 1;
			goto mcp4822_exit_close;
		}

		#if PERF_CYCLE
		#if PERF_CCNT
		CCNT_CCNT( perf_ccnt_cycle_end );
		ck_cycle[i] = CCNT_DELTA( perf_ccnt_cycle_start, perf_ccnt_cycle_end );
		#endif
		#if PERF_GETTIME
		perf_cycle_end = nano_now();
		ns_cycle[i] = perf_cycle_end - perf_cycle_start;
		#endif
		#endif
		#if PERF_CYCLE_FULL
		#if PERF_CCNT
		CCNT_CCNT( perf_ccnt_cyclefull_end );
		ck_cyclefull[i] = CCNT_DELTA( perf_ccnt_cyclefull_start, perf_ccnt_cyclefull_end );
		#endif
		#if PERF_GETTIME
		perf_cyclefull_end = nano_now();
		ns_cyclefull[i] = perf_cyclefull_end - perf_cyclefull_start;
		#endif
		#endif

		#if SCHED_YIELD
		sched_yield();
		#else
		#endif

		#if SLEEP_ONCE
		#if SLEEP == SLEEP_NANO_NOW
		nsleep_busy_up_to_get_return sr = nsleep_busy_up_to_get( in_cycle_start_ns, nsec_per_cycle );
		tot_ns_sleeped += sr.sleeped_ns;
		in_cycle_start_ns = sr.now_ns;
		#elif SLEEP == SLEEP_CCNT
		cksleep_busy_up_to_get_return sr = cksleep_busy_up_to_get( in_cycle_start_ck, ck_per_cycle );
		tot_ck_sleeped += sr.sleeped_ck;
		in_cycle_start_ck = sr.now_ck;
		#endif
		#endif
	}
	end_ns = nano_now();

	for ( mcp_index = 0; mcp_index != 2; ++mcp_index )
	{
		u8 gain = 1;
		u16 v = 0;
		u8 shutdown = 1;
		b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, v, gain, shutdown );
		if ( ! b_res )
		{
			fprintf( stderr, "error shutting down DAC pin %u\n", (uint32_t)mcp_index );
			ret = 1;
			goto mcp4822_exit_close;
		}
	}

	#if SLEEP == SLEEP_CCNT
	tot_ns_sleeped = tot_ck_sleeped * ( f64(1e9) / f64(CPU_FREQ) );
	#endif
	tot_ns = end_ns - start_ns;
	ns_ps = tot_ns / f64(TOTAL_VALUES);
	Hz = TOTAL_VALUES / ( f64(tot_ns) / 1e9 );
	fprintf( stderr, "targetting %lluns per sample (sleeped %lluns in total (%llu clock cycles) ),\n%d samples took %lluns, %fns per sample, %fHz\n",
	                 nsec_per_cycle, tot_ns_sleeped, tot_ck_sleeped, TOTAL_VALUES, tot_ns, ns_ps, Hz );

	if ( tot_ns_sleeped > tot_ns )
	{
		log_err( "tot_ns_sleeped > tot_ns" );
	}

	tot_ns -= tot_ns_sleeped;

	ns_ps = tot_ns / (f64)(TOTAL_VALUES);
	Hz = TOTAL_VALUES / ( f64(tot_ns) / 1e9 );
	fprintf( stderr, "%d samples took %lluns, %fns per sample, %fHz\n", TOTAL_VALUES, tot_ns, ns_ps, Hz );

	#if TRACE_VALUES
	{
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%f\n", values[i] );
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}
	#endif


	#if PERF_CCNT
	#if PERF_ADC_DAC
	{
		const char * out_file_path = perf_ck_adc_file_path;
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%f\n", ck_adc[i] * ck_to_ns );
			sched_yield();
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}
	{
		const char * out_file_path = perf_ck_dac_file_path;
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%f\n", ck_dac[i] * ck_to_ns );
			sched_yield();
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}
	#endif
	#if PERF_CYCLE
	{
		const char * out_file_path = perf_ck_cycle_file_path;
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%f\n", ck_cycle[i] * ck_to_ns );
			sched_yield();
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}
	#endif
	#if PERF_CYCLE_FULL
	{
		const char * out_file_path = perf_ck_cyclefull_file_path;
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%f\n", ck_cyclefull[i] * ck_to_ns );
			sched_yield();
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}
	#endif
	#endif
	#if PERF_GETTIME
	#if PERF_ADC_DAC
	{
		const char * out_file_path = perf_adc_file_path;
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%llu\n", ns_adc[i] );
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}
	{
		const char * out_file_path = perf_dac_file_path;
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%llu\n", ns_dac[i] );
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}
	#endif
	#if PERF_CYCLE
	{
		const char * out_file_path = perf_cycle_file_path;
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%llu\n", ns_cycle[i] );
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}
	#endif
	#if PERF_CYCLE_FULL
	{
		const char * out_file_path = perf_cyclefull_file_path;
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%llu\n", ns_cyclefull[i] );
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}
	#endif
	#endif

	mcp3202_exit_close:
	mcp4822_exit_close:
	b_res = spi_close( &mcp4822_dev.spi_device );
	if ( !b_res )
	{
		log_err( "failed to close mcp4822" );
	}
	mcp4822_exit_cannot_open:
	b_res = spi_close( &mcp3202_dev.spi_device );
	if ( !b_res )
	{
		log_err( "failed to close mcp3202" );
	}
	mcp3202_exit_cannot_open:

	spi_exit( SPI_Device::Lib::PIGPIO );

	cannot_set_schedule:

	return ret;
}
#include "loop_pedal.cpp"
#include "spi.cpp"
#include "mcp4822.cpp"
#include "mcp3202.cpp"
#include "tggpio.cpp"
#include "pigpio.c"
