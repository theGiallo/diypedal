#if 0
	USER=pi
	REMOTE=192.168.1.3
	#REMOTE=rpi00
	REMOTE_PATH=/home/pi/
	TMUX_TARGET=0
	REL_ROOT_PROJ=../

	THIS_FILE_PATH=$0
	THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	#THIS_FILE_DIR=${THIS_FILE_PATH%/*}
	THIS_FILE=${THIS_FILE_PATH##*/}
	EXECUTABLE_NAME=${THIS_FILE%%.*}

	#echo this file dir = "'$THIS_FILE_DIR'"
	#echo this file path = "'$THIS_FILE_PATH'"
	#echo this file = "'$THIS_FILE'"
	#echo executable name = "'$EXECUTABLE_NAME'"

	if [ -z "${CXX+x}" ]
	then
		CXX=arm-linux-gnueabihf-g++
	fi

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/include/"
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/repo/"

	declare -a LIBS_DIRS
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/lib/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	#LIBS[${#LIBS[@]}]=pigpiod_if2
	#LIBS[${#LIBS[@]}]=pigpio
	LIBS[${#LIBS[@]}]=rt

	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	$CXX -std=c++11 -fpermissive -Wall -Wextra -Wno-comment `include_dirs` `libs_dirs` `link_libs` -O3 -fPIC -o "$EXECUTABLE_NAME" "$THIS_FILE";
	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		printf 'will copy file to remote and run it\n'
		printf "user = '$USER'\n"
		printf "remote = '$REMOTE'\n"

		scp -i ../bin/pi_id_rsa "./$EXECUTABLE_NAME" $USER@$REMOTE:$REMOTE_PATH \
		&& \
		ssh -i ../bin/pi_id_rsa $USER@$REMOTE "tmux send-keys -t $TMUX_TARGET sudo\ $REMOTE_PATH$EXECUTABLE_NAME\ \"$@\" Enter"

		ret=$?

		if [ ! $ret -eq 0 ]
		then
			printf "scp failed\n"
		fi
		trash "$EXECUTABLE_NAME"
		#trash "$EXECUTABLE_NAME.o"
	else
		printf 'compilation failed\n' >&2;
	fi


	echo "exiting $ret"
	exit $ret
#endif
#include <sys/resource.h> // setpriority(...)
#include <errno.h> // perror()
#include <stdlib.h>
#include <math.h>
#include "tgsched.h"
#include "basic_types.h"
#include "macro_tools.h"
#include "tgtime.h"

#define RENICE 1
#define NICE_VALUE -20
#define DO_SCHED_DEADLINE 1
#define DO_SCHED_FIFO 0
#define SCHED_YIELD 1
#define RUN_FOR_SECS 60
int
main( int argc, char *argv[] )
{
	(void)( argc );
	(void)( argv );

	int ret = 0;

	#if DO_SCHED_DEADLINE
	{
		u32 nsec_per_cycle      = 100000;
		u32 nsec_per_cycle_work = 40000;
		//int i_ret = tgsched_deadline_self( nsec_per_cycle, nsec_per_cycle_work );

		// NOTE(theGiallo): with this values we run at avg of 45.6kHz with 60~us of max cycle
		//int i_ret = tgsched_deadline_self( 2048*8, 1024 );

		// --- // NOTE(theGiallo): with this values we run at avg of 43.3kHz with 60~us of max cycle
		// NOTE(theGiallo): with this values we run at avg of 45.6kHz with 60~us of max cycle
		//int i_ret = tgsched_deadline_self( nsec_per_cycle >> 1, nsec_per_cycle_work >> 1 );

		u32 period  = nsec_per_cycle     ;// / 8;
		u32 runtime = nsec_per_cycle_work;// / 8;
		log_info( "runtime = %d period = %d %f", runtime, period, runtime / (f32)period );
		errno = 0;
		int i_ret = tgsched_deadline_self( period, runtime );

		//do
		//{
			//int i_ret = tgsched_deadline_self( nsec_per_cycle / 6, nsec_per_cycle_work / 6 );

			if ( i_ret < 0 )
			{
				log_err( "could not set scheduler to SCHED_DEADLINE err = %d errn = %d", i_ret, errno );
				perror( "tgsched_setattr" );
				ret = -1;
				//return ret;
				//goto cannot_set_schedule;
				//exit( -1 );
			}
		//} while ( errno == 16 && (sched_yield(),true) );
	}
	#elif DO_SCHED_FIFO
	{
		errno = 0;
		int i_ret = tgsched_fifo_self();
		if ( i_ret < 0 )
		{
			log_err( "could not set scheduler to SCHED_FIFO err = %d errn = %d", i_ret, errno );
			perror( "tgsched_fifo_self" );
			ret = -1;
		}
	}
	#endif

	{
		//enable_ccr(0);
		ccnt_init();
		//u32 ccnt_s = ccnt_read();
		u32 ccnt_s, ccnt_e;
		CCNT_CCNT(ccnt_s);
		u64 nn_s = nano_now();
		CCNT_CCNT(ccnt_e);
		//u32 ccnt_e = ccnt_read();
		u64 nn_e = nano_now();
		u32 ccnt_tot = ccnt_e - ccnt_s;
		fprintf( stderr, "double nano_now() took %u ticks\n", ccnt_tot );
		u64 nn = nn_e - nn_s;
		fprintf( stderr, "double nano_now() took %lluns\n", nn );
	}

	#if RENICE
	{
		int which = PRIO_PROCESS;
		id_t pid;
		int priority = NICE_VALUE;
		int ret;

		pid = getpid();
		ret = setpriority(which, pid, priority);
		if ( ret == -1 )
		{
			log_err( "failed to renice itself to %d", priority );
			perror( 0 );
		}
	}
	#endif

	{
		u32 expected_ns = 22675;
		s32 cycles_count = 60 * f64(CPU_CLOCK_FREQ) / u64( expected_ns * 3 );
		u32 ck_prev = 0;
		for ( int i = 0; i != cycles_count; ++i )
		{
			u32 ck;
			CCNT_CCNT( ck );
			if ( ck < ck_prev )
			{
				u32 d = CCNT_DELTA( ck_prev, ck );
				log_info( "%u -> %u [ %x -> %x ] delta = %u [%x]", ck_prev, ck, ck_prev, ck, d, d );
			}
			ck_prev = ck;

			u32 ck_start, ck_end;
			u64 ns_start = nano_now();
			CCNT_CCNT(ck_start);
			cksleep_busy( CPU_CLOCK_FROM_NS( expected_ns ) );
			CCNT_CCNT(ck_end);
			u64 ns_end = nano_now();
			u64 ns = ns_end - ns_start;
			u32 ck_delta = CCNT_DELTA(ck_start,ck_end);
			u32 ck_delta_ns = u32(NS_FROM_CPU_CLOCK(ck_delta));
			f32 ratio = f32(ck_delta_ns) / f32(expected_ns);
			if ( ratio < 0.9 || ratio > 4.0f )
			{
				log_info( "ratio = %f", ratio );
				log_info( "nano_now measured %llu ns", ns );
				log_info( "ccnt measured %u ck, or %u ns", ck_delta, ck_delta_ns );
			}

			u32 ck_start_2, ck_end_2;
			CCNT_CCNT(ck_start_2);
			cksleep_busy_up_to_get_return r =
			   cksleep_busy_up_to_get( ck_start, expected_ns * 2 );
			CCNT_CCNT(ck_end_2);
			ck_delta = CCNT_DELTA(ck_start_2,ck_end_2);
			ck_delta_ns = u32(NS_FROM_CPU_CLOCK(ck_delta));
			if ( ck_delta > 0xffffffff / 2 )
			{
				log_info( "cksleep_busy_up_to_get sleeped_ck = %u", r.sleeped_ck );
				log_info( "ccnt measured %u ck, or %u ns", ck_delta, ck_delta_ns );
			}

			sched_yield();
		}
	}

	#if DO_SCHED_DEADLINE || DO_SCHED_FIFO
	cannot_set_schedule:
	#endif

	return ret;
}
