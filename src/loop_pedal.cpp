#include "tgtime.h"

void
loop_pedal_init( Loop_Pedal * loop_pedal, u32 max_samples )
{
	zero_struct( *loop_pedal );
	loop_pedal->max_samples = max_samples;
	loop_pedal->record_dck  = (u32*) malloc( max_samples * sizeof( u32 ) );
	loop_pedal->record_volt = (f32*) malloc( max_samples * sizeof( f32 ) );
}
#define NEXT_PLAY_INDEX() \
	(loop_pedal->play_index >= loop_pedal->record_count ? 0 : loop_pedal->play_index + 1);
#define ADVANCE_PLAY_INDEX() \
	loop_pedal->play_index = NEXT_PLAY_INDEX();

static
inline
f32
loop_pedal_interpolate( Loop_Pedal * loop_pedal, u32 curr_play_ck, s32 next_play_index )
{
	f32 ret;// = LOOP_PEDAL_VZERO;
	f32 A = loop_pedal->record_volt[loop_pedal->play_index];
	f32 B = loop_pedal->record_volt[next_play_index];
	u32 next_sample_dck = loop_pedal->record_dck[next_play_index];
	f32 t = curr_play_ck / f32(next_sample_dck);
	ret = A + ( B - A ) * t;
	return ret;
}

void
loop_pedal_cycle( Loop_Pedal * loop_pedal, f32 in_voltage, f32 * out_voltage )
{
	u32 ck_end;
	CCNT_CCNT( ck_end );
	u32 ck_cycle = CCNT_DELTA( loop_pedal->last_cycle_ck_start, ck_end );
	loop_pedal->last_cycle_ck_start = ck_end;

	f32 v = in_voltage;

	if ( loop_pedal->is_recording )
	{
		s32 i = loop_pedal->record_index;
		loop_pedal->record_dck [i] = ck_cycle;
		loop_pedal->record_volt[i] = in_voltage;
		loop_pedal->record_count = ++loop_pedal->record_index;
		if ( loop_pedal->record_index == loop_pedal->max_samples )
		{
			loop_pedal->is_recording = 0;
			loop_pedal->is_playing   = 1;
			loop_pedal->play_index   = 0;
			loop_pedal->record_index = 0;
			loop_pedal->play_dck     = 0;
		}
	} else
	if ( loop_pedal->is_playing && loop_pedal->record_count )
	{
		f32 rec_v;

		s32 next_play_index = NEXT_PLAY_INDEX();
		u32 next_sample_dck = loop_pedal->record_dck[next_play_index];
		u32 last_play_ck    = loop_pedal->play_dck;
		u32 curr_play_ck    = loop_pedal->play_dck + ck_cycle;
		while ( curr_play_ck >= next_sample_dck )
		{
			curr_play_ck -= next_sample_dck;

			loop_pedal->play_index = next_play_index;

			next_play_index = NEXT_PLAY_INDEX();
			next_sample_dck = loop_pedal->record_dck[next_play_index];
		}
		u32 prev_sample = loop_pedal->record_volt[loop_pedal->play_index];
		u32 next_sample = loop_pedal->record_volt[next_play_index];
		f32 t = curr_play_ck / f32(next_sample_dck);
		rec_v = loop_pedal_interpolate( loop_pedal, curr_play_ck, next_play_index );
		loop_pedal->play_dck = curr_play_ck;


		#if LOOP_PEDAL_VZERO_UNKNOWN
		// NOTE(theGiallo): we mix via division because we don't know the zero level of the signal, the virtual ground
		v = ( in_voltage + rec_v ) / 2;
		#else
		v = LOOP_PEDAL_VZERO + ( in_voltage - LOOP_PEDAL_VZERO + rec_v - LOOP_PEDAL_VZERO );
		#endif
	}
	*out_voltage = v;
}
#undef ADVANCE_PLAY_INDEX
