#if 0
	USER=pi
	REMOTE=192.168.1.3
	#REMOTE=rpi00
	REMOTE_PATH=/home/pi/
	TMUX_TARGET=0
	REL_ROOT_PROJ=../

	THIS_FILE_PATH=$0
	THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	#THIS_FILE_DIR=${THIS_FILE_PATH%/*}
	THIS_FILE=${THIS_FILE_PATH##*/}
	EXECUTABLE_NAME=${THIS_FILE%%.*}

	#echo this file dir = "'$THIS_FILE_DIR'"
	#echo this file path = "'$THIS_FILE_PATH'"
	#echo this file = "'$THIS_FILE'"
	#echo executable name = "'$EXECUTABLE_NAME'"

	if [ -z "${CXX+x}" ]
	then
		CXX=arm-linux-gnueabihf-g++
	fi

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/include/"

	declare -a LIBS_DIRS
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/lib/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	#LIBS[${#LIBS[@]}]=pigpiod_if2
	LIBS[${#LIBS[@]}]=pigpio
	LIBS[${#LIBS[@]}]=rt

	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	$CXX -std=c++11 -Wall -Wextra -Wno-comment `include_dirs` `libs_dirs` `link_libs` -O3 -fPIC -o "$EXECUTABLE_NAME" "$THIS_FILE";
	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		printf 'will copy file to remote and run it\n'
		printf "user = '$USER'\n"
		printf "remote = '$REMOTE'\n"

		scp -i ../bin/pi_id_rsa "./$EXECUTABLE_NAME" $USER@$REMOTE:$REMOTE_PATH \
		&& \
		ssh -i ../bin/pi_id_rsa $USER@$REMOTE "tmux send-keys -t $TMUX_TARGET sudo\ $REMOTE_PATH$EXECUTABLE_NAME\ \"$@\" Enter"

		ret=$?

		if [ ! $ret -eq 0 ]
		then
			printf "scp failed\n"
		fi
		trash "$EXECUTABLE_NAME"
		#trash "$EXECUTABLE_NAME.o"
	else
		printf 'compilation failed\n' >&2;
	fi


	echo "exiting $ret"
	exit $ret
#endif
#include <stdlib.h>
#include "basic_types.h"
#include "macro_tools.h"
#include "tgtime.h"
#include "mcp4822.h"
#include "mcp3202.h"
#include "tggpio.h"

int
main( int argc, char *argv[] )
{
	(void)( argc );
	(void)( argv );

	int ret = 0;

	char * out_file_path = 0;
	if ( argc == 2 )
	{
		out_file_path = argv[1];
	}

	//const int OUT_PINS_COUNT = 2;
	const int VALUES_COUNT = 44100 * 3;
	const int TOTAL_VALUES = VALUES_COUNT;
	//const f32 VOLTAGE_MAX = MCP4822_SPI::OUT_MAX_V;
	f32 * values = (f32*)calloc( sizeof ( f32 ), VALUES_COUNT );


	u64 start_ns, end_ns, tot_ns, tot_ns_sleeped = 0;
	f64 ns_ps, Hz;
	//#if 1
	//u64 usec_sleep = 1e6f / 44100.0f; // 22.6757
	//#else
	//u64 usec_sleep = 1000f;
	//#endif
	u64 in_cycle_start_ns = 0, out_cycle_start_ns = 0, nsec_per_cycle = 1e9 / 44100.0;

	float   voltage   = 0;
	uint8_t mcp_index = 0;

	spi_init( SPI_Device::Lib::PIGPIO );
	tggpio_setup();

	MCP3202_SPI mcp3202_dev;
	mcp3202_init( &mcp3202_dev );

	MCP4822_SPI mcp4822_dev;
	mcp4822_init( &mcp4822_dev, 0xFF );

	mcp3202_dev.spi_device.SPI_number    = 0;
	mcp3202_dev.spi_device.SPI_channel   = 0;

	// NOTE(theGiallo): freq max is 0.9MHz with VDD=2.7V and 1.8 with VDD=5V
	#if 1
	mcp3202_dev.spi_device.speed_hz      = 1800000;
	//mcp3202_dev.reference_voltage        = 5.0f;
	mcp3202_dev.reference_voltage        = 4.096f;
	//mcp3202_dev.reference_voltage        = 4.100f;
	#else
	mcp3202_dev.spi_device.speed_hz      = 900000;
	mcp3202_dev.reference_voltage        = 3.33f;
	#endif

	bool b_res;
	b_res = spi_open( &mcp3202_dev.spi_device );
	if ( !b_res )
	{
		ret = 1;
		goto mcp3202_exit_cannot_open;
	}

	mcp4822_dev.spi_device.SPI_number    = 0;
	mcp4822_dev.spi_device.SPI_channel   = 1;

	b_res = spi_open( &mcp4822_dev.spi_device );
	if ( !b_res )
	{
		ret = 1;
		goto mcp4822_exit_cannot_open;
	}


	voltage = 1.024f;
	mcp_index = 0;
	out_cycle_start_ns = nano_now();
	b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, voltage );
	if ( ! b_res )
	{
		fprintf( stderr, "error outputting voltage from pin %u\n", (uint32_t)mcp_index );
		ret = 1;
		goto mcp4822_exit_close;
	}
	//nsleep_busy( 100 );

	start_ns = nano_now();
	for ( int i = 0; i != VALUES_COUNT; ++i )
	{
		voltage = 0;
		mcp_index = 0;

		tot_ns_sleeped += nsleep_busy_up_to_get( in_cycle_start_ns, nsec_per_cycle );
		in_cycle_start_ns = nano_now();
		b_res = mcp3202_spi_read_value( &mcp3202_dev, mcp_index, &voltage );
		if ( ! b_res )
		{
			fprintf( stderr, "error reading voltage from pin %u\n", (uint32_t)mcp_index );
			ret = 1;
			goto mcp3202_exit_close;
		} else
		{
			values[i] = voltage;
		}

		tot_ns_sleeped += nsleep_busy_up_to_get( out_cycle_start_ns, nsec_per_cycle );
		out_cycle_start_ns = nano_now();
		b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, voltage );
		if ( ! b_res )
		{
			fprintf( stderr, "error outputting voltage from pin %u\n", (uint32_t)mcp_index );
			ret = 1;
			goto mcp4822_exit_close;
		}

		//nsleep_busy_up_to( cycle_start_ns, nsec_per_cycle );
	}
	end_ns = nano_now();

	for ( mcp_index = 0; mcp_index != 2; ++mcp_index )
	{
		u8 gain = 0;
		u16 v = 0;
		u8 shutdown = 1;
		b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, v, gain, shutdown );
		if ( ! b_res )
		{
			fprintf( stderr, "error shutting down DAC pin %u\n", (uint32_t)mcp_index );
			ret = 1;
			goto mcp4822_exit_close;
		}
	}

	tot_ns = end_ns - start_ns;
	ns_ps = tot_ns / f64(TOTAL_VALUES);
	Hz = TOTAL_VALUES / ( f64(tot_ns) / 1e9 );
	//tot_ns_sleeped = usec_sleep * u64(VALUES_COUNT * 1000);
	fprintf( stderr, "targetting %lluns per sample (sleeped %lluns in total), %d samples took %lluns, %fns per sample, %fHz\n",
	                 nsec_per_cycle, tot_ns_sleeped, TOTAL_VALUES, tot_ns, ns_ps, Hz );

	if ( tot_ns_sleeped > tot_ns )
	{
		log_err( "tot_ns_sleeped > tot_ns" );
	}

	tot_ns -= tot_ns_sleeped;
	ns_ps = tot_ns / (f64)(TOTAL_VALUES);
	Hz = TOTAL_VALUES / ( f64(tot_ns) / 1e9 );
	fprintf( stderr, "%d samples took %lluns, %fns per sample, %fHz\n", TOTAL_VALUES, tot_ns, ns_ps, Hz );

	{
		FILE * out_fd = stdout;
		if ( out_file_path )
		{
			out_fd = fopen( out_file_path, "w+" );
			if ( ! out_fd )
			{
				log_err( "failed to open '%s', outputting to stdout", out_file_path );
				out_fd = stdout;
			}
		}
		for ( int i = 0; i != VALUES_COUNT; ++i )
		{
			fprintf( out_fd, "%f\n", values[i] );
		}
		if ( out_fd != stdout )
		{
			fclose( stdout );
		}
	}

	mcp3202_exit_close:
	mcp4822_exit_close:
	b_res = spi_close( &mcp4822_dev.spi_device );
	if ( !b_res )
	{
		log_err( "failed to close mcp4822" );
	}
	mcp4822_exit_cannot_open:
	b_res = spi_close( &mcp3202_dev.spi_device );
	if ( !b_res )
	{
		log_err( "failed to close mcp3202" );
	}
	mcp3202_exit_cannot_open:

	spi_exit( SPI_Device::Lib::PIGPIO );

	return ret;
}
#include "spi.cpp"
#include "mcp4822.cpp"
#include "mcp3202.cpp"
#include "tggpio.cpp"
