#ifndef __MCP3008_H__
#define __MCP3008_H__ 1

#include <stdio.h>
#if MCP3008_TEST
#endif
#include "spi.h"
#include "macro_tools.h"

// NOTE(theGiallo): freq max is 1.35MHz with VDD=2.7V and 3.6 with VDD=5V
// NOTE(theGiallo): freq min is 10kHz
// NOTE(theGiallo): SPI mode can be 0 or 3
// NOTE(theGiallo): CS must be high before and after acquisition,
// pigpio sets the CS pin to active and to inactive, before and after comm.
// At spiOpen it set it to active and then inactive.
struct
MCP3008_SPI
{
	SPI_Device spi_device;
	float      reference_voltage;
};
bool
mcp3008_spi_read_value( MCP3008_SPI * dev, uint8_t index, uint16_t * out_value );
// NOTE(theGiallo): difference couples are always odd = 1 + even, e.g. 0+ 1- or 0- 1+.
bool
mcp3008_spi_read_diff_value( MCP3008_SPI * dev, uint8_t index_positive, uint16_t * out_value );
inline
float
mcp3008_spi_sample_to_volt( MCP3008_SPI * dev, uint16_t sample );
bool
mcp3008_spi_read_value( MCP3008_SPI * dev, uint8_t index, float * voltage );
bool
mcp3008_spi_read_diff_value( MCP3008_SPI * dev, uint8_t index_positive, float * voltage );

#if MCP3008_TEST
int
mcp3008_test( int argc, char *argv[] );
#endif

#endif /* ifndef __MCP3008_H__ */
