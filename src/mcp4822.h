#ifndef __MCP4822_H__
#define __MCP4822_H__ 1

#include "spi.h"

// NOTE(theGiallo): SPI 3-wire
// NOTE(theGiallo): SPI mode can be 0 or 3
// NOTE(theGiallo): CS must be high before and after acquisition,
// pigpio sets the CS pin to active and to inactive, before and after comm.
// At spiOpen it set it to active and then inactive.
struct
MCP4822_SPI
{
	SPI_Device spi_device;
	uint8_t    LDAC_pin;
	static constexpr float OUT_MAX_V = 2.048f; // NOTE(theGiallo): with no gain
};

void
mcp4822_sync_output( MCP4822_SPI * dev );
bool
mcp4822_spi_output_value( MCP4822_SPI * dev, uint8_t index, uint16_t value_12bit, uint8_t gain = 0, uint8_t shutdown = 0 );
bool
mcp4822_spi_output_value( MCP4822_SPI * dev, uint8_t index, float voltage, uint8_t shutdown = 0 );
bool
mcp4822_spi_output_value_double_precision( MCP4822_SPI * dev, uint32_t value_24bit );
bool
mcp4822_spi_output_value_double_precision( MCP4822_SPI * dev, float voltage_value );
inline
float
mcp4822_spi_decode_volt( uint16_t sample, uint8_t gain );
inline
uint16_t
mcp4822_spi_encode_volt( float voltage, uint8_t gain );
inline
uint32_t
mcp4822_spi_encode_volt_24bit( float voltage, uint8_t gain );
void
mcp4822_init( MCP4822_SPI * mcp4822_dev, uint8_t LDAC_pin );

#if MCP4822_SPI_TEST
int
mcp4822_test( int argc, char * argv[] );
#endif

#endif /* ifndef __MCP4822_H__ */

