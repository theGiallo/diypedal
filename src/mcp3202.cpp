#if MCP3202_SPI_TEST
#include <stdlib.h>
#include "tgtime.h"
#include "basic_types.h"
#endif
#include "mcp3202.h"
#include "macro_tools.h"

bool
mcp3202_spi_read_value( MCP3202_SPI * dev, uint8_t index, uint16_t * out_value )
{
	bool ret = mcp3202_spi_read_value( dev, 1, index, out_value );

	return ret;
}
bool
mcp3202_spi_read_diff_value( MCP3202_SPI * dev, uint8_t index, uint16_t * out_value )
{
	bool ret = mcp3202_spi_read_value( dev, 0, index, out_value );

	return ret;
}
bool
mcp3202_spi_read_value( MCP3202_SPI * dev, uint8_t sgl1_diff0, uint8_t index, uint16_t * out_value )
{
	bool ret = false;

	tg_assert( index == 0 || index == 1 );

	// NOTE(theGiallo):  Most Significative Bits First
	uint8_t msbf = 1;

	// NOTE(theGiallo):
	//    |               |                                        |                               |
	// TX: 0 0 0 0 0 0 0 1 SGL/DIFF ODD/SIGN MSBF 0  0   0   0   0   0   0   0   0   0   0   0   0
	// RX: 0 0 0 0 0 0 0 0    0        0      0   0 b11 b10 b09 b08 b07 b06 b05 b04 b03 b02 b01 b00
	uint8_t buf_tx[3] = { 0x1, uint8_t( ( sgl1_diff0 << 7 ) | ( index << 6 ) | ( msbf << 5 ) ), 0x00 };
	uint8_t buf_rx[3] = {};
	bool b_res = spi_write_read( &dev->spi_device, buf_tx, buf_rx, sizeof ( buf_tx ) );
	if ( !b_res )
	{
		return ret;
	}

	uint16_t v2 = ( ( 0x0f & buf_rx[1] ) << 8 ) | buf_rx[2];

	*out_value = v2;
	ret = true;

	return ret;
}
inline
float
mcp3202_spi_sample_to_volt( MCP3202_SPI * dev, uint16_t sample )
{
	#if MCP3202_DOUBLE_PRECISION
	float ret = float( double( sample ) * dev->reference_voltage / double( 0x0fff ) );
	#else
	float ret = float( sample ) * dev->reference_voltage / float( 0x0fff );
	#endif
	return ret;
}
bool
mcp3202_spi_read_value( MCP3202_SPI * dev, uint8_t sgl1_diff0, uint8_t index, float * voltage )
{
	bool ret = false;
	uint16_t sample = 0;
	bool b_res = mcp3202_spi_read_value( dev, sgl1_diff0, index, &sample );
	if ( !b_res )
	{
		return ret;
	}
	#if 0
	printf( "sample = %u\n", uint32_t( sample ) );
	#endif
	*voltage = mcp3202_spi_sample_to_volt( dev, sample );
	ret = true;
	return ret;
}
bool
mcp3202_spi_read_value( MCP3202_SPI * dev, uint8_t index, float * voltage )
{
	bool ret = mcp3202_spi_read_value( dev, 1, index, voltage );
	return ret;
}
bool
mcp3202_spi_read_diff_value( MCP3202_SPI * dev, uint8_t index, float * voltage )
{
	bool ret = mcp3202_spi_read_value( dev, 0, index, voltage );
	return ret;
}
void
mcp3202_init( MCP3202_SPI * mcp3202_dev )
{
	zero_struct( *mcp3202_dev );
	mcp3202_dev->spi_device.mode          = SPI_MODE_0;
	mcp3202_dev->spi_device.bits_per_word = 8;
}

#if MCP3202_SPI_TEST
int
mcp3202_test( int argc, char *argv[] )
{
	(void)( argc );
	(void)( argv );

	int ret = 0;

	const int VALUES_COUNT = 64000 * 3;
	f32 * values = (f32*)calloc( sizeof ( f32 ), VALUES_COUNT );

	spi_init( SPI_Device::Lib::PIGPIO );

	MCP3202_SPI mcp3202_dev;
	mcp3202_init( &mcp3202_dev );

	// NOTE(theGiallo): set by init function
	//mcp3202_dev.spi_device.mode          = SPI_MODE_0;
	//mcp3202_dev.spi_device.bits_per_word = 8;

	mcp3202_dev.spi_device.SPI_number    = 0;
	mcp3202_dev.spi_device.SPI_channel   = 0;

	// NOTE(theGiallo): freq max is 0.9MHz with VDD=2.7V and 1.8 with VDD=5V
	#if MCP3202_TEST_V_5
	mcp3202_dev.spi_device.speed_hz      = 1800000;
	mcp3202_dev.reference_voltage        = 5.0f;
	#elif MCP3202_TEST_V_3_3
	mcp3202_dev.spi_device.speed_hz      = 900000;
	mcp3202_dev.reference_voltage        = 3.3f;
	#else
	#error "must define either MCP3202_TEST_V_5 or MCP3202_TEST_V_3_3"
	#endif
	//mcp3202_dev.reference_voltage        = 1.0f;

	u64 start_ns, end_ns, tot_ns, tot_ns_sleeped;
	f64 ns_ps, Hz;
	u64 usec_sleep = 1e6f / 44100.0f;

	float   voltage   = 0;
	uint8_t mcp_index = 0;
	bool b_res;
	b_res = spi_open( &mcp3202_dev.spi_device );
	if ( !b_res )
	{
		ret = 1;
		goto mcp3202_test_exit_cannot_open;
	}

	start_ns = nano_now();
	for ( int i = 0; i != VALUES_COUNT; ++i )
	{
		voltage = 0;
		mcp_index = 0;
		b_res = mcp3202_spi_read_value( &mcp3202_dev, mcp_index, &voltage );
		if ( b_res )
		{
			//printf( "pin %d read voltage = %fV\n", mcp_index, voltage );
			//printf( "%f\n", voltage );
			values[i] = voltage;
		} else
		{
			fprintf( stderr, "error reading voltage from pin %u\n", (uint32_t)mcp_index );
			ret = 1;
			goto mcp3202_test_exit_close;
		}
		usleep_busy( usec_sleep );
	}
	end_ns = nano_now();
	tot_ns = end_ns - start_ns;
	ns_ps = tot_ns / (f64)VALUES_COUNT;
	Hz = VALUES_COUNT / ((f64)tot_ns/1e9);
	tot_ns_sleeped = usec_sleep * u64(VALUES_COUNT * 1000);
	fprintf( stderr, "sleeping %lluus per sample (%lluns in total), %d samples took %lluns, %fns per sample, %fHz\n",
	                 usec_sleep, tot_ns_sleeped, VALUES_COUNT, tot_ns, ns_ps, Hz );

	if ( tot_ns_sleeped > tot_ns )
	{
		log_err( "tot_ns_sleeped > tot_ns" );
	}

	tot_ns -= tot_ns_sleeped;
	ns_ps = tot_ns / (f64)VALUES_COUNT;
	Hz = VALUES_COUNT / ((f64)tot_ns/1e9);
	fprintf( stderr, "%d samples took %lluns, %fns per sample, %fHz\n", VALUES_COUNT, tot_ns, ns_ps, Hz );
	for ( int i = 0; i != VALUES_COUNT; ++i )
	{
		printf( "%f\n", values[i] );
	}
	#if 0
	mcp_index = 1;
	b_res = mcp3202_spi_read_value( &mcp3202_dev, mcp_index, &voltage );
	if ( b_res )
	{
		printf( "pin %d read voltage = %fV\n", mcp_index, voltage );
	} else
	{
		fprintf( stderr, "error reading voltage from pin %u\n", (uint32_t)mcp_index );
		ret = 1;
		goto mcp3202_test_exit_close;
	}
	#endif

	mcp3202_test_exit_close:
	b_res = spi_close( &mcp3202_dev.spi_device );
	mcp3202_test_exit_cannot_open:

	spi_exit( SPI_Device::Lib::PIGPIO );

	return ret;
}
#endif
