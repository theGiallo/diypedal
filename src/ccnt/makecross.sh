#!/bin/bash
if [ -z "$XCOMP_PATH" ];
then
	echo "please define XCOMP_PATH"
	exit 1
fi
export ARCH=arm
export CROSS_COMPILE=$XCOMP_PATH/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-
export INSTALL_MOD_PATH=$XCOMP_PATH/rt-kernel
export INSTALL_DTBS_PATH=$XCOMP_PATH/rt-kernel

make -f Makefile.cross
