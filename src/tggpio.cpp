#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "tggpio.h"
#include "tgtime.h"

#define TGGPIO_USE_PIGPIO 1
void
tggpio_button_setup( tggpio_Button * button, u8 bcm_gpio_pin )
{
	tg_assert( tggpio_is_pin_valid( bcm_gpio_pin ) );

	#if TGGPIO_USE_PIGPIO
	gpioSetMode( bcm_gpio_pin, PI_INPUT );
	gpioSetPullUpDown( bcm_gpio_pin, PI_PUD_UP );
	#else
	TGGPIO_INP( bcm_gpio_pin );

	tggpio_set_pull( bcm_gpio_pin, tggpio_Pull::PULL_UP );
	#endif

	button->bcm_gpio_pin = bcm_gpio_pin;
	button->status = tggpio_Button::Status::UP;
	button->front  = tggpio_Button::Front::STATIC;
}

void
tggpio_button_read( tggpio_Button * button )
{
	u32 ck_now;
	CCNT_CCNT( ck_now );
	u8 v;
	#if TGGPIO_USE_PIGPIO
	v = gpioRead( button->bcm_gpio_pin );
	#else
	v = TGGPIO_GET( button->bcm_gpio_pin );
	#endif
	tggpio_Button::Status curr_status = v == 0 ? tggpio_Button::Status::DOWN : tggpio_Button::Status::UP;
	if ( button->status != curr_status )
	{
		button->front = curr_status == tggpio_Button::Status::UP ? tggpio_Button::Front::UP : tggpio_Button::Front::DOWN;
		if ( button->front == tggpio_Button::Front::DOWN )
		{
			button->start_ck_down = ck_now;
		} else
		{
			button->start_ck_up = ck_now;
		}
	} else
	{
		button->front = tggpio_Button::Front::STATIC;
	}
	if ( curr_status == tggpio_Button::Status::DOWN )
	{
		button->last_ck_down = ck_now;
	} else
	{
		button->last_ck_up = ck_now;
	}

	button->status = curr_status;
}

#if TGGPIO_EXAMPLE
void
tggpio_example_print_button( int g )
{
	if ( TGGPIO_GET( g ) ) // !=0 <-> bit is 1 <- port is HIGH=3.3V
	{
		printf( "Button pressed!\n" );
	} else // port is LOW=0V
	{
		printf( "Button released!\n" );
	}
}

int
tggpio_example( int argc, char ** argv )
{
	int g, rep;

	// Set up gpi pointer for direct register access
	if ( ! tggpio_setup() )
	{
		return 1;
	}

	// Switch GPIO 7..11 to output mode

	/************************************************************************\
	 * You are about to change the GPIO settings of your computer.          *
	 * Mess this up and it will stop working!                               *
	 * It might be a good idea to 'sync' before running this program        *
	 * so at least you still have your code changes written to the SD-card! *
	 \************************************************************************/

	// Set GPIO pins 7-11 to output
	for ( g = 7; g <= 11; ++g )
	{
		TGGPIO_INP( g ); // must use TGGPIO_INP before we can use TGGPIO_OUT
		TGGPIO_OUT( g );
	}

	for ( rep = 0; rep < 10; ++rep )
	{
		for ( g = 7; g <= 11; ++g )
		{
			TGGPIO_SET = 1 << g;
			sleep( 1 );
		}
		for ( g =7; g <= 11; g++ )
		{
			TGGPIO_CLR = 1 << g;
			sleep( 1 );
		}
	}

	return 0;

} // main
#endif


//
// Set up a memory regions to access GPIO
//
bool
tggpio_setup()
{
	bool ret = false;

	/* open /dev/mem */
	if ( ( tggpio_mem_fd = open( "/dev/mem", O_RDWR|O_SYNC ) ) < 0 )
	{
		printf( "can't open /dev/mem \n" );
		//exit( -1 );
		return ret;
	}

	/* mmap GPIO */
	tggpio_map = mmap(
	   NULL,                // Any adddress in our space will do
	   TGGPIO_BLOCK_SIZE,   // Map length
	   PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
	   MAP_SHARED,          // Shared with other processes
	   tggpio_mem_fd,       // File to map
	   TGGPIO_GPIO_BASE     // Offset to GPIO peripheral
	);

	close( tggpio_mem_fd ); //No need to keep tggpip_mem_fd open after mmap

	if ( tggpio_map == MAP_FAILED )
	{
		printf( "mmap error %d\n", (int)tggpio_map );//errno also set!
		return ret;
		//exit( -1 ));
	}

	// Always use volatile pointer!
	tggpio = (volatile unsigned *)tggpio_map;
	ret = true;
	return ret;
} // setup_io
