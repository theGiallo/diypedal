#ifndef __LOOP_PEDAL_H__
#define __LOOP_PEDAL_H__ 1

#ifndef LOOP_PEDAL_VZERO
#define LOOP_PEDAL_VZERO 2.048f
#endif

struct
Loop_Pedal
{
	f32 * record_volt;
	u32 * record_dck;
	s32   max_samples;
	s32   record_count;
	s32   record_index;

	s32   play_index;
	u32   play_dck;

	u8 is_recording;
	u8 is_playing;

	u32 last_cycle_ck_start;
};

void
loop_pedal_init( Loop_Pedal * loop_pedal, u32 max_samples );

void
loop_pedal_cycle( Loop_Pedal * loop_pedal, f32 in_voltage, f32 * out_voltage );

#endif /* ifndef __LOOP_PEDAL_H__ */
