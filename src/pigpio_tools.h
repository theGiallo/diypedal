#ifndef __PIGPIO_TOOLS_H__
#define __PIGPIO_TOOLS_H__ 1

#include <pigpio.h>

#define IF_ERR_LOG_PIGPIO_ERR_AND_EXIT(err,exit) \
	if ( err < 0 ) \
	{ \
		fprintf( stderr, "err = %d\n", err, pigpio_error( err ) ); \
		goto exit; \
	} \

inline
const char *
pigpio_errstr( int err )
{
	const char * ret = 0;
	switch ( err )
	{
		case PI_INIT_FAILED       :/*-1*/ // gpioInitialise failed
			ret = "gpioInitialise failed";
			break;
		case PI_BAD_USER_GPIO     :/*-2*/ // GPIO not 0-31
			ret = "GPIO not 0-31";
			break;
		case PI_BAD_GPIO          :/*-3*/ // GPIO not 0-53
			ret = "GPIO not 0-53";
			break;
		case PI_BAD_MODE          :/*-4*/ // mode not 0-7
			ret = "mode not 0-7";
			break;
		case PI_BAD_LEVEL         :/*-5*/ // level not 0-1
			ret = "level not 0-1";
			break;
		case PI_BAD_PUD           :/*-6*/ // pud not 0-2
			ret = "pud not 0-2";
			break;
		case PI_BAD_PULSEWIDTH    :/*-7*/ // pulsewidth not 0 or 500-2500
			ret = "pulsewidth not 0 or 500-2500";
			break;
		case PI_BAD_DUTYCYCLE     :/*-8*/ // dutycycle outside set range
			ret = "dutycycle outside set range";
			break;
		case PI_BAD_TIMER         :/*-9*/ // timer not 0-9
			ret = "timer not 0-9";
			break;
		case PI_BAD_MS           :/*-10*/ // ms not 10-60000
			ret = "ms not 10-60000";
			break;
		case PI_BAD_TIMETYPE     :/*-11*/ // timetype not 0-1
			ret = "timetype not 0-1";
			break;
		case PI_BAD_SECONDS      :/*-12*/ // seconds < 0
			ret = "seconds < 0";
			break;
		case PI_BAD_MICROS       :/*-13*/ // micros not 0-999999
			ret = "micros not 0-999999";
			break;
		case PI_TIMER_FAILED     :/*-14*/ // gpioSetTimerFunc failed
			ret = "gpioSetTimerFunc failed";
			break;
		case PI_BAD_WDOG_TIMEOUT :/*-15*/ // timeout not 0-60000
			ret = "timeout not 0-60000";
			break;
		//case PI_NO_ALERT_FUNC    :/*-16*/ // DEPRECATED
		//	ret = "DEPRECATED";
		//	break;
		case PI_BAD_CLK_PERIPH   :/*-17*/ // clock peripheral not 0-1
			ret = "clock peripheral not 0-1";
			break;
		//case PI_BAD_CLK_SOURCE   :/*-18*/ // DEPRECATED
		//	ret = "DEPRECATED";
		//	break;
		case PI_BAD_CLK_MICROS   :/*-19*/ // clock micros not 1, 2, 4, 5, 8, or 10
			ret = "clock micros not 1, 2, 4, 5, 8, or 10";
			break;
		case PI_BAD_BUF_MILLIS   :/*-20*/ // buf millis not 100-10000
			ret = "buf millis not 100-10000";
			break;
		case PI_BAD_DUTYRANGE    :/*-21*/ // dutycycle range not 25-40000
			ret = "dutycycle range not 25-40000";
			break;
		//case PI_BAD_DUTY_RANGE   :/*-21*/ // DEPRECATED (use PI_BAD_DUTYRANGE)
		//	ret = "DEPRECATED (use PI_BAD_DUTYRANGE)";
		//	break;
		case PI_BAD_SIGNUM       :/*-22*/ // signum not 0-63
			ret = "signum not 0-63";
			break;
		case PI_BAD_PATHNAME     :/*-23*/ // can't open pathname
			ret = "can't open pathname";
			break;
		case PI_NO_HANDLE        :/*-24*/ // no handle available
			ret = "no handle available";
			break;
		case PI_BAD_HANDLE       :/*-25*/ // unknown handle
			ret = "unknown handle";
			break;
		case PI_BAD_IF_FLAGS     :/*-26*/ // ifFlags > 4
			ret = "ifFlags > 4";
			break;
		case PI_BAD_CHANNEL      :/*-27*/ // DMA channel not 0-15
			ret = "DMA channel not 0-15";
			break;
		//case PI_BAD_PRIM_CHANNEL :/*-27*/ // DMA primary channel not 0-15
		//	ret = "DMA primary channel not 0-15";
		//	break;
		case PI_BAD_SOCKET_PORT  :/*-28*/ // socket port not 1024-32000
			ret = "socket port not 1024-32000";
			break;
		case PI_BAD_FIFO_COMMAND :/*-29*/ // unrecognized fifo command
			ret = "unrecognized fifo command";
			break;
		case PI_BAD_SECO_CHANNEL :/*-30*/ // DMA secondary channel not 0-15
			ret = "DMA secondary channel not 0-15";
			break;
		case PI_NOT_INITIALISED  :/*-31*/ // function called before gpioInitialise
			ret = "function called before gpioInitialise";
			break;
		case PI_INITIALISED      :/*-32*/ // function called after gpioInitialise
			ret = "function called after gpioInitialise";
			break;
		case PI_BAD_WAVE_MODE    :/*-33*/ // waveform mode not 0-3
			ret = "waveform mode not 0-3";
			break;
		case PI_BAD_CFG_INTERNAL :/*-34*/ // bad parameter in gpioCfgInternals call
			ret = "bad parameter in gpioCfgInternals call";
			break;
		case PI_BAD_WAVE_BAUD    :/*-35*/ // baud rate not 50-250K(RX)/50-1M(TX)
			ret = "baud rate not 50-250K(RX)/50-1M(TX)";
			break;
		case PI_TOO_MANY_PULSES  :/*-36*/ // waveform has too many pulses
			ret = "waveform has too many pulses";
			break;
		case PI_TOO_MANY_CHARS   :/*-37*/ // waveform has too many chars
			ret = "waveform has too many chars";
			break;
		case PI_NOT_SERIAL_GPIO  :/*-38*/ // no bit bang serial read on GPIO
			ret = "no bit bang serial read on GPIO";
			break;
		case PI_BAD_SERIAL_STRUC :/*-39*/ // bad (null) serial structure parameter
			ret = "bad (null) serial structure parameter";
			break;
		case PI_BAD_SERIAL_BUF   :/*-40*/ // bad (null) serial buf parameter
			ret = "bad (null) serial buf parameter";
			break;
		case PI_NOT_PERMITTED    :/*-41*/ // GPIO operation not permitted
			ret = "GPIO operation not permitted";
			break;
		case PI_SOME_PERMITTED   :/*-42*/ // one or more GPIO not permitted
			ret = "one or more GPIO not permitted";
			break;
		case PI_BAD_WVSC_COMMND  :/*-43*/ // bad WVSC subcommand
			ret = "bad WVSC subcommand";
			break;
		case PI_BAD_WVSM_COMMND  :/*-44*/ // bad WVSM subcommand
			ret = "bad WVSM subcommand";
			break;
		case PI_BAD_WVSP_COMMND  :/*-45*/ // bad WVSP subcommand
			ret = "bad WVSP subcommand";
			break;
		case PI_BAD_PULSELEN     :/*-46*/ // trigger pulse length not 1-100
			ret = "trigger pulse length not 1-100";
			break;
		case PI_BAD_SCRIPT       :/*-47*/ // invalid script
			ret = "invalid script";
			break;
		case PI_BAD_SCRIPT_ID    :/*-48*/ // unknown script id
			ret = "unknown script id";
			break;
		case PI_BAD_SER_OFFSET   :/*-49*/ // add serial data offset > 30 minutes
			ret = "add serial data offset > 30 minutes";
			break;
		case PI_GPIO_IN_USE      :/*-50*/ // GPIO already in use
			ret = "GPIO already in use";
			break;
		case PI_BAD_SERIAL_COUNT :/*-51*/ // must read at least a byte at a time
			ret = "must read at least a byte at a time";
			break;
		case PI_BAD_PARAM_NUM    :/*-52*/ // script parameter id not 0-9
			ret = "script parameter id not 0-9";
			break;
		case PI_DUP_TAG          :/*-53*/ // script has duplicate tag
			ret = "script has duplicate tag";
			break;
		case PI_TOO_MANY_TAGS    :/*-54*/ // script has too many tags
			ret = "script has too many tags";
			break;
		case PI_BAD_SCRIPT_CMD   :/*-55*/ // illegal script command
			ret = "illegal script command";
			break;
		case PI_BAD_VAR_NUM      :/*-56*/ // script variable id not 0-149
			ret = "script variable id not 0-149";
			break;
		case PI_NO_SCRIPT_ROOM   :/*-57*/ // no more room for scripts
			ret = "no more room for scripts";
			break;
		case PI_NO_MEMORY        :/*-58*/ // can't allocate temporary memory
			ret = "can't allocate temporary memory";
			break;
		case PI_SOCK_READ_FAILED :/*-59*/ // socket read failed
			ret = "socket read failed";
			break;
		case PI_SOCK_WRIT_FAILED :/*-60*/ // socket write failed
			ret = "socket write failed";
			break;
		case PI_TOO_MANY_PARAM   :/*-61*/ // too many script parameters (> 10)
			ret = "too many script parameters (> 10)";
			break;
		//case PI_NOT_HALTED       :/*-62*/ // DEPRECATED
		//	ret = "DEPRECATED";
		//	break;
		case PI_SCRIPT_NOT_READY :/*-62*/ // script initialising
			ret = "script initialising";
			break;
		case PI_BAD_TAG          :/*-63*/ // script has unresolved tag
			ret = "script has unresolved tag";
			break;
		case PI_BAD_MICS_DELAY   :/*-64*/ // bad MICS delay (too large)
			ret = "bad MICS delay (too large)";
			break;
		case PI_BAD_MILS_DELAY   :/*-65*/ // bad MILS delay (too large)
			ret = "bad MILS delay (too large)";
			break;
		case PI_BAD_WAVE_ID      :/*-66*/ // non existent wave id
			ret = "non existent wave id";
			break;
		case PI_TOO_MANY_CBS     :/*-67*/ // No more CBs for waveform
			ret = "No more CBs for waveform";
			break;
		case PI_TOO_MANY_OOL     :/*-68*/ // No more OOL for waveform
			ret = "No more OOL for waveform";
			break;
		case PI_EMPTY_WAVEFORM   :/*-69*/ // attempt to create an empty waveform
			ret = "attempt to create an empty waveform";
			break;
		case PI_NO_WAVEFORM_ID   :/*-70*/ // no more waveforms
			ret = "no more waveforms";
			break;
		case PI_I2C_OPEN_FAILED  :/*-71*/ // can't open I2C device
			break;
		case PI_SER_OPEN_FAILED  :/*-72*/ // can't open serial device
			break;
		case PI_SPI_OPEN_FAILED  :/*-73*/ // can't open SPI device
			break;
		case PI_BAD_I2C_BUS      :/*-74*/ // bad I2C bus
			ret = "bad I2C bus";
			break;
		case PI_BAD_I2C_ADDR     :/*-75*/ // bad I2C address
			ret = "bad I2C address";
			break;
		case PI_BAD_SPI_CHANNEL  :/*-76*/ // bad SPI channel
			ret = "bad SPI channel";
			break;
		case PI_BAD_FLAGS        :/*-77*/ // bad i2c/spi/ser open flags
			ret = "bad i2c/spi/ser open flags";
			break;
		case PI_BAD_SPI_SPEED    :/*-78*/ // bad SPI speed
			ret = "bad SPI speed";
			break;
		case PI_BAD_SER_DEVICE   :/*-79*/ // bad serial device name
			ret = "bad serial device name";
			break;
		case PI_BAD_SER_SPEED    :/*-80*/ // bad serial baud rate
			ret = "bad serial baud rate";
			break;
		case PI_BAD_PARAM        :/*-81*/ // bad i2c/spi/ser parameter
			ret = "bad i2c/spi/ser parameter";
			break;
		case PI_I2C_WRITE_FAILED :/*-82*/ // i2c write failed
			ret = "i2c write failed";
			break;
		case PI_I2C_READ_FAILED  :/*-83*/ // i2c read failed
			ret = "i2c read failed";
			break;
		case PI_BAD_SPI_COUNT    :/*-84*/ // bad SPI count
			ret = "bad SPI count";
			break;
		case PI_SER_WRITE_FAILED :/*-85*/ // ser write failed
			ret = "ser write failed";
			break;
		case PI_SER_READ_FAILED  :/*-86*/ // ser read failed
			ret = "ser read failed";
			break;
		case PI_SER_READ_NO_DATA :/*-87*/ // ser read no data available
			ret = "ser read no data available";
			break;
		case PI_UNKNOWN_COMMAND  :/*-88*/ // unknown command
			ret = "unknown command";
			break;
		case PI_SPI_XFER_FAILED  :/*-89*/ // spi xfer/read/write failed
			ret = "spi xfer/read/write failed";
			break;
		case PI_BAD_POINTER      :/*-90*/ // bad (NULL) pointer
			ret = "bad (NULL) pointer";
			break;
		case PI_NO_AUX_SPI       :/*-91*/ // no auxiliary SPI on Pi A or B
			ret = "no auxiliary SPI on Pi A or B";
			break;
		case PI_NOT_PWM_GPIO     :/*-92*/ // GPIO is not in use for PWM
			ret = "GPIO is not in use for PWM";
			break;
		case PI_NOT_SERVO_GPIO   :/*-93*/ // GPIO is not in use for servo pulses
			ret = "GPIO is not in use for servo pulses";
			break;
		case PI_NOT_HCLK_GPIO    :/*-94*/ // GPIO has no hardware clock
			ret = "GPIO has no hardware clock";
			break;
		case PI_NOT_HPWM_GPIO    :/*-95*/ // GPIO has no hardware PWM
			ret = "GPIO has no hardware PWM";
			break;
		case PI_BAD_HPWM_FREQ    :/*-96*/ // invalid hardware PWM frequency
			ret = "invalid hardware PWM frequency";
			break;
		case PI_BAD_HPWM_DUTY    :/*-97*/ // hardware PWM dutycycle not 0-1M
			ret = "hardware PWM dutycycle not 0-1M";
			break;
		case PI_BAD_HCLK_FREQ    :/*-98*/ // invalid hardware clock frequency
			ret = "invalid hardware clock frequency";
			break;
		case PI_BAD_HCLK_PASS    :/*-99*/ // need password to use hardware clock 1
			ret = "need password to use hardware clock 1";
			break;
		case PI_HPWM_ILLEGAL    :/*-100*/ // illegal, PWM in use for main clock
			ret = "illegal, PWM in use for main clock";
			break;
		case PI_BAD_DATABITS    :/*-101*/ // serial data bits not 1-32
			ret = "serial data bits not 1-32";
			break;
		case PI_BAD_STOPBITS    :/*-102*/ // serial (half) stop bits not 2-8
			ret = "serial (half) stop bits not 2-8";
			break;
		case PI_MSG_TOOBIG      :/*-103*/ // socket/pipe message too big
			ret = "socket/pipe message too big";
			break;
		case PI_BAD_MALLOC_MODE :/*-104*/ // bad memory allocation mode
			ret = "bad memory allocation mode";
			break;
		case PI_TOO_MANY_SEGS   :/*-105*/ // too many I2C transaction segments
			ret = "too many I2C transaction segments";
			break;
		case PI_BAD_I2C_SEG     :/*-106*/ // an I2C transaction segment failed
			ret = "an I2C transaction segment failed";
			break;
		case PI_BAD_SMBUS_CMD   :/*-107*/ // SMBus command not supported by driver
			ret = "SMBus command not supported by driver";
			break;
		case PI_NOT_I2C_GPIO    :/*-108*/ // no bit bang I2C in progress on GPIO
			ret = "no bit bang I2C in progress on GPIO";
			break;
		case PI_BAD_I2C_WLEN    :/*-109*/ // bad I2C write length
			ret = "bad I2C write length";
			break;
		case PI_BAD_I2C_RLEN    :/*-110*/ // bad I2C read length
			ret = "bad I2C read length";
			break;
		case PI_BAD_I2C_CMD     :/*-111*/ // bad I2C command
			ret = "bad I2C command";
			break;
		case PI_BAD_I2C_BAUD    :/*-112*/ // bad I2C baud rate, not 50-500k
			ret = "bad I2C baud rate, not 50-500k";
			break;
		case PI_CHAIN_LOOP_CNT  :/*-113*/ // bad chain loop count
			ret = "bad chain loop count";
			break;
		case PI_BAD_CHAIN_LOOP  :/*-114*/ // empty chain loop
			ret = "empty chain loop";
			break;
		case PI_CHAIN_COUNTER   :/*-115*/ // too many chain counters
			ret = "too many chain counters";
			break;
		case PI_BAD_CHAIN_CMD   :/*-116*/ // bad chain command
			ret = "bad chain command";
			break;
		case PI_BAD_CHAIN_DELAY :/*-117*/ // bad chain delay micros
			ret = "bad chain delay micros";
			break;
		case PI_CHAIN_NESTING   :/*-118*/ // chain counters nested too deeply
			ret = "chain counters nested too deeply";
			break;
		case PI_CHAIN_TOO_BIG   :/*-119*/ // chain is too long
			ret = "chain is too long";
			break;
		case PI_DEPRECATED      :/*-120*/ // deprecated function removed
			ret = "deprecated function removed";
			break;
		case PI_BAD_SER_INVERT  :/*-121*/ // bit bang serial invert not 0 or 1
			ret = "bit bang serial invert not 0 or 1";
			break;
		case PI_BAD_EDGE        :/*-122*/ // bad ISR edge value, not 0-2
			ret = "bad ISR edge value, not 0-2";
			break;
		case PI_BAD_ISR_INIT    :/*-123*/ // bad ISR initialisation
			ret = "bad ISR initialisation";
			break;
		case PI_BAD_FOREVER     :/*-124*/ // loop forever must be last command
			ret = "loop forever must be last command";
			break;
		case PI_BAD_FILTER      :/*-125*/ // bad filter parameter
			ret = "bad filter parameter";
			break;
		case PI_BAD_PAD         :/*-126*/ // bad pad number
			ret = "bad pad number";
			break;
		case PI_BAD_STRENGTH    :/*-127*/ // bad pad drive strength
			ret = "bad pad drive strength";
			break;
		case PI_FIL_OPEN_FAILED :/*-128*/ // file open failed
			ret = "file open failed";
			break;
		case PI_BAD_FILE_MODE   :/*-129*/ // bad file mode
			ret = "bad file mode";
			break;
		case PI_BAD_FILE_FLAG   :/*-130*/ // bad file flag
			ret = "bad file flag";
			break;
		case PI_BAD_FILE_READ   :/*-131*/ // bad file read
			ret = "bad file read";
			break;
		case PI_BAD_FILE_WRITE  :/*-132*/ // bad file write
			ret = "bad file write";
			break;
		case PI_FILE_NOT_ROPEN  :/*-133*/ // file not open for read
			ret = "file not open for read";
			break;
		case PI_FILE_NOT_WOPEN  :/*-134*/ // file not open for write
			ret = "file not open for write";
			break;
		case PI_BAD_FILE_SEEK   :/*-135*/ // bad file seek
			ret = "bad file seek";
			break;
		case PI_NO_FILE_MATCH   :/*-136*/ // no files match pattern
			ret = "no files match pattern";
			break;
		case PI_NO_FILE_ACCESS  :/*-137*/ // no permission to access file
			ret = "no permission to access file";
			break;
		case PI_FILE_IS_A_DIR   :/*-138*/ // file is a directory
			ret = "file is a directory";
			break;
		case PI_BAD_SHELL_STATUS :/*-139*/ // bad shell return status
			ret = "bad shell return status";
			break;
		case PI_BAD_SCRIPT_NAME :/*-140*/ // bad script name
			ret = "bad script name";
			break;
		case PI_BAD_SPI_BAUD    :/*-141*/ // bad SPI baud rate, not 50-500k
			ret = "bad SPI baud rate, not 50-500k";
			break;
		case PI_NOT_SPI_GPIO    :/*-142*/ // no bit bang SPI in progress on GPIO
			ret = "no bit bang SPI in progress on GPIO";
			break;
		case PI_BAD_EVENT_ID    :/*-143*/ // bad event id
			ret = "bad event id";
			break;
		case PI_CMD_INTERRUPTED :/*-144*/ // Used by Python
			ret = "Used by Python";
			break;
		case PI_NOT_ON_BCM2711  :/*-145*/ // not available on BCM2711
			ret = "not available on BCM2711";
			break;
		case PI_ONLY_ON_BCM2711 :/*-146*/ // only available on BCM2711
			ret = "only available on BCM2711";
			break;

		case PI_PIGIF_ERR_0    :/*-2000*/ 
			ret = "PI_PIGIF_ERR_0";
			break;
		case PI_PIGIF_ERR_99   :/*-2099*/ 
			ret = "PI_PIGIF_ERR_99";
			break;

		case PI_CUSTOM_ERR_0   :/*-3000*/ 
			ret = "PI_CUSTOM_ERR_0";
			break;
		case PI_CUSTOM_ERR_999 :/*-3999*/ 
			ret = "PI_CUSTOM_ERR_999";
			break;
		default:
			ret = "unknown error";
			break;
	}

	return ret;
}

#endif /* ifndef __PIGPIO_TOOLS_H__ */
