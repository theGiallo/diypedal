#include <stdio.h>
#if MCP3008_TEST
#endif
#include "spi.h"
#include "macro_tools.h"

bool
mcp3008_spi_read_value( MCP3008_SPI * dev, uint8_t sgl1_diff0, uint8_t index, uint16_t * out_value )
{
	bool ret = false;

	uint8_t buf_tx[3] = { 0x1, ( sgl1_diff0 << 7 ) | ( ( index & 0x07 ) << 4 ) ), 0x00 };
	uint8_t buf_rx[3] = {};
	bool b_res = spi_write_read( &dev->spi_device, buf_tx, buf_rx, sizeof ( buf_tx ) );
	if ( !b_res )
	{
		return ret;
	}
	#if 0
	for ( int i = 0; i != 3; ++i )
	{
		printf( "buf[%d] = 0x%02x\n", i, uint32_t( buf[i] ) );
	}
	#endif

	uint16_t v2 = ( ( 0x03 & buf_rx[1] ) << 8 ) | buf_rx[2];

	// NOTE(theGiallo): this is as shown in
	// http://www.hertaville.com/interfacing-an-spi-adc-mcp3008-chip-to-the-raspberry-pi-using-c.html
	// but it's empirically wrong on my RPi0
	#if 0
	uint16_t v = buf[1] & 0b11000000;
	v = ( v << 8 ) | buf[2];
	printf( "v2 = %u\n", uint32_t( v2 ) );
	printf( "v  = %u\n", uint32_t( v  ) );
	#endif
	*out_value = v2;
	ret = true;

	return ret;
}
bool
mcp3008_spi_read_value( MCP3008_SPI * dev, uint8_t index, uint16_t * out_value )
{
	bool ret = mcp3008_spi_read_value( dev, 1, index, out_value );

	return ret;
}
// NOTE(theGiallo): difference couples are always odd = 1 + even, e.g. 0+ 1- or 0- 1+.
bool
mcp3008_spi_read_diff_value( MCP3008_SPI * dev, uint8_t index_positive, uint16_t * out_value )
{
	bool ret = mcp3008_spi_read_value( dev, 0, index, out_value );

	return ret;
}
inline
float
mcp3008_spi_sample_to_volt( MCP3008_SPI * dev, uint16_t sample )
{
	float ret = float( sample ) * dev->reference_voltage / float( 0x03ff );
	return ret;
}
bool
mcp3008_spi_read_value( MCP3008_SPI * dev, uint8_t sgl1_diff0, uint8_t index, float * voltage )
{
	bool ret = false;
	uint16_t sample = 0;
	bool b_res = mcp3008_spi_read_value( dev, sgl1_diff0, index, &sample );
	if ( !b_res )
	{
		return ret;
	}
	#if 0
	printf( "sample = %u\n", uint32_t( sample ) );
	#endif
	*voltage = mcp3008_spi_sample_to_volt( dev, sample );
	ret = true;
	return ret;
}
bool
mcp3008_spi_read_value( MCP3008_SPI * dev, uint8_t index, float * voltage )
{
	bool ret = mcp3008_spi_read_value( dev, 1, index, voltage );
	return ret;
}
bool
mcp3008_spi_read_diff_value( MCP3008_SPI * dev, uint8_t index_positive, float * voltage )
{
	bool ret = mcp3008_spi_read_value( dev, 0, index_positive, voltage );
	return ret;
}

void
mpc3008_init( MCP3008_SPI * mcp3008_dev )
{
	zero_struct( *mcp3008_dev );
	mcp3008_dev->mode          = SPI_MODE_0;
	mcp3008_dev->bits_per_word = 8;
}

#if MCP3008_TEST
int
mcp3008_test( int argc, char *argv[] )
{
	(void)( argc );
	(void)( argv );

	int ret = 0;
	MCP3008_SPI mcp3008_dev;
	mcp3008_init( &mcp3008_dev );

	// NOTE(theGiallo): set by init function
	//mcp3008_dev.spi_device.mode          = SPI_MODE_0;
	//mcp3008_dev.spi_device.bits_per_word = 8;

	mcp3008_dev.spi_device.SPI_number    = 0;
	mcp3008_dev.spi_device.SPI_channel   = 0;

	// NOTE(theGiallo): freq max is 1.35MHz with VDD=2.7V and 3.6 with VDD=5V
	#if MCP3008_TEST_V_5
	mcp3008_dev.spi_device.speed_hz      = 3600000;
	mcp3008_dev.reference_voltage        = 5.0f;
	#elif MCP3008_TEST_V_3_3
	mcp3008_dev.spi_device.speed_hz      = 1350000;
	mcp3008_dev.reference_voltage        = 3.3f;
	#endif
	//mcp3008_dev.reference_voltage        = 1.0f;

	float   voltage   = 0;
	uint8_t mcp_index = 0;
	bool b_res;
	b_res = spi_open( &mcp3008_dev.spi_device );
	if ( !b_res )
	{
		ret = 1;
		goto mcp3008_test_exit_cannot_open;
	}

	for ( int i = 0; i != 8000; ++i )
	{
		voltage = 0;
		mcp_index = 0;
		b_res = mcp3008_spi_read_value( &mcp3008_dev, mcp_index, &voltage );
		if ( b_res )
		{
			//printf( "pin %d read voltage = %fV\n", mcp_index, voltage );
			printf( "%f\n", voltage );
		} else
		{
			fprintf( stderr, "error reading voltage from pin %u\n", (uint32_t)mcp_index );
			ret = 1;
			goto mcp3008_test_exit_close;
		}
		usleep( 1000 );
	}
	#if 0
	mcp_index = 1;
	b_res = mcp3008_spi_read_value( &mcp3008_dev, mcp_index, &voltage );
	if ( b_res )
	{
		printf( "pin %d read voltage = %fV\n", mcp_index, voltage );
	} else
	{
		fprintf( stderr, "error reading voltage from pin %u\n", (uint32_t)mcp_index );
		ret = 1;
		goto mcp3008_test_exit_close;
	}
	#endif

	mcp3008_test_exit_close:
	b_res = spi_close( &mcp3008_dev.spi_device );
	mcp3008_test_exit_cannot_open:
	return ret;
}
#endif
