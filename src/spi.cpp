#include <stdio.h>
#include <unistd.h>
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>

#include <pigpio.h>

#include "macro_tools.h"
#include "pigpio_tools.h"

const char * SPI_Device::spi_devs[2][3] = { { "/dev/spidev0.0", "/dev/spidev0.1", "" },
                                            { "/dev/spidev1.0", "/dev/spidev1.1", "/dev/spidev1.2" },
};
SPI_Device::GPIO_Pins SPI_Device::gpio_pins[2] =
{
	{                 // BCM    ID
		.CE0_GPIO_pin  =  8, // 24
		.CE1_GPIO_pin  =  7, // 26
		.CE2_GPIO_pin  = 0xff,
		.MOSI_GPIO_pin = 10, // 19
		.MISO_GPIO_pin =  9, // 21
		.SCLK_GPIO_pin = 11, // 23
	},
	{
		.CE0_GPIO_pin  = 18, // 12
		.CE1_GPIO_pin  = 17, // 11
		.CE2_GPIO_pin  = 16, // 36
		.MOSI_GPIO_pin = 20, // 38
		.MISO_GPIO_pin = 19, // 35
		.SCLK_GPIO_pin = 21, // 40
	}
};

bool
spi_init( SPI_Device::Lib lib )
{
	bool ret = false;
	switch ( lib )
	{
		case SPI_Device::Lib::PIGPIO:
		{
			int res = gpioInitialise();
			ret = res >= 0;
			break;
		}
		case SPI_Device::Lib::KERNEL_DRIVER:
			ret = true;
			break;
		default:
			break;
	}
	return ret;
}
void
spi_exit( SPI_Device::Lib lib )
{
	switch ( lib )
	{
		case SPI_Device::Lib::PIGPIO:
		{
			gpioTerminate();
			break;
		}
		case SPI_Device::Lib::KERNEL_DRIVER:
			break;
		default:
			break;
	}
}

bool
spi_open( SPI_Device * spi_dev )
{
	bool ret = false;
	if ( spi_dev->SPI_number > 1 )
	{
		fprintf( stderr, "invalid SPI number: can only be 0 or 1\n" );
		return ret;
	}
	if ( spi_dev->SPI_number == 0 && spi_dev->SPI_channel > 1 )
	{
		fprintf( stderr, "invalid SPI channel(%d): dev 0 has 2 channels\n", spi_dev->SPI_channel );
		return ret;
	}
	if ( spi_dev->SPI_number == 1 && spi_dev->SPI_channel > 2 )
	{
		fprintf( stderr, "invalid SPI channel(%d): dev 0 has 3 channels\n", spi_dev->SPI_channel );
		return ret;
	}

	switch ( (SPI_Device::Lib)spi_dev->lib )
	{
		case SPI_Device::Lib::PIGPIO:
		{
			uint32_t flags = 0;
			flags |= spi_dev->mode;
			flags |= spi_dev->active_high_CE0        <<  2;
			flags |= spi_dev->active_high_CE1        <<  3;
			flags |= spi_dev->active_high_CE2        <<  4;
			flags |= spi_dev->not_reserved_spi_CE0   <<  5;
			flags |= spi_dev->not_reserved_spi_CE1   <<  6;
			flags |= spi_dev->not_reserved_spi_CE2   <<  7;
			flags |= spi_dev->SPI_number             <<  8;
			flags |= spi_dev->is_3_wire              <<  9;
			flags |= spi_dev->bytes_MOSI_MISO_switch << 10;
			flags |= spi_dev->ls_bit_MOSI_first      << 14;
			flags |= spi_dev->ls_bit_MISO_first      << 15;
			flags |= spi_dev->bits_per_word          << 16;
			int res;
			res = spiOpen( (unsigned)spi_dev->SPI_channel, (unsigned)spi_dev->speed_hz, flags );
			if ( res < 0 )
			{
				log_err( "failed to spiOpen SPI %d channel %d. Err = %s", spi_dev->SPI_number, spi_dev->SPI_channel, pigpio_errstr( res ) );
			} else
			{
				spi_dev->fd = res;
				log_dbg( "spiOpen returned %d", spi_dev->fd );
				ret = true;
			}

			break;
		}
		case SPI_Device::Lib::KERNEL_DRIVER:
		{
			spi_dev->fd = open( SPI_Device::spi_devs[spi_dev->SPI_number][spi_dev->SPI_channel], O_RDWR );
			#if 0
			printf( "opened spi_dev->fg = %d\n", spi_dev->fd );
			#endif
			if ( spi_dev->fd < 0 )
			{
				perror( "could not open SPI device" );
				return ret;
			}

			int spi_fd = spi_dev->fd;
			int status_val;

			{
				uint8_t mode = spi_dev->mode;
				status_val = ioctl( spi_fd, SPI_IOC_WR_MODE, &mode );
				if ( status_val < 0 )
				{
					perror( "ioctl failed: could not set SPI mode WR" );
					return ret;
				}
				status_val = ioctl( spi_fd, SPI_IOC_RD_MODE, &mode );
				if ( status_val < 0 )
				{
					perror( "ioctl failed: could not set SPI mode RD" );
					return ret;
				}
			}
			{
				uint8_t bits_per_word = spi_dev->bits_per_word;
				status_val = ioctl( spi_fd, SPI_IOC_WR_BITS_PER_WORD, &bits_per_word );
				if ( status_val < 0 )
				{
					perror( "ioctl failed: could not set SPI WR bits per word" );
					return ret;
				}
				status_val = ioctl( spi_fd, SPI_IOC_RD_BITS_PER_WORD, &bits_per_word );
				if ( status_val < 0 )
				{
					perror( "ioctl failed: could not set SPI RD bits per word" );
					return ret;
				}
			}
			status_val = ioctl( spi_fd, SPI_IOC_WR_MAX_SPEED_HZ, &spi_dev->speed_hz );
			if ( status_val < 0 )
			{
				perror( "ioctl failed: could not set SPI WR speed Hz" );
				return ret;
			}
			status_val = ioctl( spi_fd, SPI_IOC_RD_MAX_SPEED_HZ, &spi_dev->speed_hz );
			if ( status_val < 0 )
			{
				perror( "ioctl failed: could not set SPI RD speed Hz" );
				return ret;
			}

			ret = true;

			break;
		}
	}


	return ret;
}
bool
spi_close( SPI_Device * spi_dev )
{
	bool ret = false;

	switch ( (SPI_Device::Lib)spi_dev->lib )
	{
		case SPI_Device::Lib::PIGPIO:
		{
			int res;
			res = spiClose( spi_dev->fd );
			if ( res < 0 )
			{
				log_err( "failed to spiClose SPI %d channel %d. Err = %s", spi_dev->SPI_number, spi_dev->SPI_channel, pigpio_errstr( res ) );
			} else
			{
				spi_dev->fd = res;
				ret = true;
			}
			break;
		}
		case SPI_Device::Lib::KERNEL_DRIVER:
		{
			int status_val = close( spi_dev->fd );
			if ( status_val < 0 )
			{
				perror( "could not close SPI device" );
			} else
			{
				ret = true;
				spi_dev->fd = 0;
			}
			break;
		}
	}
	return ret;
}
bool
spi_read( SPI_Device * spi_dev, uint8_t * buf_rx, int length )
{
	bool ret = false;
	switch ( (SPI_Device::Lib)spi_dev->lib )
	{
		case SPI_Device::Lib::PIGPIO:
		{
			int res;
			res = spiRead( spi_dev->fd, (char*)buf_rx, length );
			if ( res < 0 )
			{
				log_err( "failed to spiread SPI %d channel %d of %d bytes. Err = %s",
				         spi_dev->SPI_number, spi_dev->SPI_channel, length, pigpio_errstr( res ) );
			} else
			{
				ret = true;
			}

			break;
		}
		case SPI_Device::Lib::KERNEL_DRIVER:
		{
			log_err( "spi_read SPI_Device::Lib::KERNEL_DRIVER not implemented" );
			ILLEGAL_PATH();
			break;
		}
	}
	return ret;
}
bool
spi_write( SPI_Device * spi_dev, uint8_t * buf_tx, int length )
{
	bool ret = false;
	switch ( (SPI_Device::Lib)spi_dev->lib )
	{
		case SPI_Device::Lib::PIGPIO:
		{
			int res;
			res = spiWrite( spi_dev->fd, (char*)buf_tx, length );
			if ( res < 0 )
			{
				log_err( "failed to spiWrite SPI %d channel %d of %d bytes. Err = %s",
				         spi_dev->SPI_number, spi_dev->SPI_channel, length, pigpio_errstr( res ) );
			} else
			{
				ret = true;
			}

			break;
		}
		case SPI_Device::Lib::KERNEL_DRIVER:
		{
			log_err( "spi_write SPI_Device::Lib::KERNEL_DRIVER not implemented" );
			ILLEGAL_PATH();
			break;
		}
	}
	return ret;
}
bool
spi_write_read( SPI_Device * spi_dev, uint8_t * buf_tx, uint8_t * buf_rx, int length )
{
	bool ret = false;

	#if 0
	printf( "spi_write_read( SPI_Device * spi_dev = 0x%032x, uint8_t * buf = 0x%032x, int length = %d )\n",
	        spi_dev, buf, length );
	printf( "spi_dev->speed_hz = %u\n", spi_dev->speed_hz );
	printf( "spi_dev->bits_per_word = %u\n", uint32_t( spi_dev->bits_per_word ) );
	printf( "spi_dev->fg = %d\n", spi_dev->fd );
	#endif

	switch ( (SPI_Device::Lib)spi_dev->lib )
	{
		case SPI_Device::Lib::PIGPIO:
		{
			int res;
			res = spiXfer( spi_dev->fd, (char*)buf_tx, (char*)buf_rx, length );
			if ( res < 0 )
			{
				log_err( "failed to spiXfer SPI %d channel %d of %d bytes. Err = %s",
				         spi_dev->SPI_number, spi_dev->SPI_channel, length, pigpio_errstr( res ) );
			} else
			{
				ret = true;
			}

			break;
		}
		case SPI_Device::Lib::KERNEL_DRIVER:
		{
			static struct spi_ioc_transfer spi[1024];
			if ( (size_t)length >= sizeof ( spi ) / sizeof ( spi[0] ) )
			{
				log_err( "(size_t)length(=%d) >= sizeof ( spi )(=%d) / sizeof ( spi[0] )(=%d)",
				         length, sizeof(spi), sizeof(spi[0]) );
				return ret;
			}
			for ( int i = 0; i != length; ++i )
			{
				spi[i].tx_buf        = (unsigned long)( buf_tx + i );
				spi[i].rx_buf        = (unsigned long)( buf_rx + i );
				spi[i].len           = sizeof ( * ( buf_tx + i ) );
				spi[i].delay_usecs   = 0;
				spi[i].speed_hz      = spi_dev->speed_hz;
				spi[i].bits_per_word = spi_dev->bits_per_word;
				spi[i].cs_change     = 0;
			}
			int res = ioctl( spi_dev->fd, SPI_IOC_MESSAGE( length ), &spi );
			if ( res < 0 )
			{
				perror( "ioctl error: failed to transmit SPI data" );
				log_err( "    data of length %u, res = %d", length, res );
			} else
			{
				ret = true;
			}
			break;
		}
		default:
			ILLEGAL_PATH();
			break;
	}

	return ret;
}
bool
spi_write_read( SPI_Device * spi_dev, uint8_t * buf_tx_rx, int length )
{
	bool ret = false;

	#if 0
	printf( "spi_write_read( SPI_Device * spi_dev = 0x%032x, uint8_t * buf = 0x%032x, int length = %d )\n",
	        spi_dev, buf, length );
	printf( "spi_dev->speed_hz = %u\n", spi_dev->speed_hz );
	printf( "spi_dev->bits_per_word = %u\n", uint32_t( spi_dev->bits_per_word ) );
	printf( "spi_dev->fg = %d\n", spi_dev->fd );
	#endif

	tg_assert( spi_dev->lib == SPI_Device::Lib::KERNEL_DRIVER );

	static struct spi_ioc_transfer spi[1024];
	if ( (size_t)length >= sizeof ( spi ) / sizeof ( spi[0] ) )
	{
		log_err( "(size_t)length(=%d) >= sizeof ( spi )(=%d) / sizeof ( spi[0] )(=%d)", length, sizeof(spi), sizeof(spi[0]) );
		return ret;
	}
	for ( int i = 0; i != length; ++i )
	{
		spi[i].tx_buf        = (unsigned long)( buf_tx_rx + i );
		spi[i].rx_buf        = (unsigned long)( buf_tx_rx + i );
		spi[i].len           = sizeof ( * ( buf_tx_rx + i ) );
		spi[i].delay_usecs   = 0;
		spi[i].speed_hz      = spi_dev->speed_hz;
		spi[i].bits_per_word = spi_dev->bits_per_word;
		spi[i].cs_change     = 0;
	}
	int res = ioctl( spi_dev->fd, SPI_IOC_MESSAGE( length ), &spi );
	if ( res < 0 )
	{
		perror( "ioctl error: failed to transmit SPI data" );
		fprintf( stderr, "    data of length %u, res = %d\n", length, res );
	} else
	{
		ret = true;
	}

	return ret;
}
