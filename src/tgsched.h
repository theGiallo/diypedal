#ifndef __TGSCHED__H__
#define __TGSCHED__H__ 1

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <linux/unistd.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <sys/syscall.h>
#include <pthread.h>

#include "macro_tools.h"

#define tg_gettid() syscall(__NR_gettid)

#define TGSCHED_DEADLINE 0x06
#define TGSCHED_FLAG_RECLAIM 0x02

/* XXX use the proper syscall numbers */
#ifdef __x86_64__
#define __NR_sched_setattr 314
#define __NR_sched_getattr 315
#endif

#ifdef __i386__
#define __NR_sched_setattr 351
#define __NR_sched_getattr 352
#endif

#ifdef __arm__
#define __NR_sched_setattr 380
#define __NR_sched_getattr 381
#endif

struct
tgsched_attr
{
	__u32 size;

	__u32 sched_policy;
	__u64 sched_flags;

	/* SCHED_NORMAL, SCHED_BATCH */
	__s32 sched_nice;

	/* SCHED_FIFO, SCHED_RR */
	__u32 sched_priority;

	/* SCHED_DEADLINE (nsec) */
	__u64 sched_runtime;
	__u64 sched_deadline;
	__u64 sched_period;
};

inline
int
tgsched_setattr( pid_t pid,
                 const struct tgsched_attr *attr,
                 unsigned int flags )
{
	return syscall( __NR_sched_setattr, pid, attr, flags );
}

inline
int
tgsched_getattr( pid_t pid,
                 struct tgsched_attr *attr,
                 unsigned int size,
                 unsigned int flags )
{
	return syscall( __NR_sched_getattr, pid, attr, size, flags );
}

inline
int
tgsched_fifo_self()
{
	struct sched_param param;
	zero_struct( param );

	param.sched_priority = sched_get_priority_max( SCHED_FIFO );

	int ret = sched_setscheduler( 0, SCHED_FIFO, &param );

	return ret;
}
inline
int
tgsched_deadline_self( __u32 period_ns, __u32 work_ns )
{
	//tg_assert( period_ns >= deadline );
	tgsched_attr attr;
	zero_struct( attr );
	attr.size = sizeof attr;
	attr.sched_policy = TGSCHED_DEADLINE;
	attr.sched_flags = TGSCHED_FLAG_RECLAIM;

	attr.sched_priority = sched_get_priority_max( TGSCHED_DEADLINE );

	attr.sched_runtime = work_ns;
	attr.sched_period = attr.sched_deadline = period_ns;

	__u32 flags = 0;
	int ret = tgsched_setattr( 0, &attr, flags );
	return ret;
}

#endif /* ifndef __TGSCHED__H__  */
