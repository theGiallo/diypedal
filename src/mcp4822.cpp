#if MCP4822_SPI_TEST
#include <stdlib.h>
#include <math.h>
#include "tgtime.h"
#include "basic_types.h"
#include "tggpio.h"
#endif
#include "mcp4822.h"
#include "macro_tools.h"

void
mcp4822_sync_output( MCP4822_SPI * dev )
{
	TGGPIO_CLRI( dev->LDAC_pin );
	TGGPIO_SETI( dev->LDAC_pin );
}

bool
mcp4822_spi_output_value( MCP4822_SPI * dev, uint8_t index, float voltage, uint8_t shutdown )
{
	tg_assert( voltage >= 0 );
	tg_assert( index == 0 || index == 1 );
	tg_assert( shutdown == 0 || shutdown == 1 );

	uint8_t gain = voltage > MCP4822_SPI::OUT_MAX_V ? 2 : 1;
	uint16_t value_12bit = mcp4822_spi_encode_volt( voltage, gain );
	bool ret = mcp4822_spi_output_value( dev, index, value_12bit, gain, shutdown );
	return ret;
}
bool
mcp4822_spi_output_value( MCP4822_SPI * dev, uint8_t index, uint16_t value_12bit, uint8_t gain, uint8_t shutdown )
{
	bool ret = false;

	tg_assert( index == 0 || index == 1 );
	tg_assert( gain == 1 || gain == 2 );
	tg_assert( shutdown == 0 || shutdown == 1 );
	shutdown = ~shutdown;

	// NOTE(theGiallo):
	//    |                             |                               |
	// TX: A/B - GA SHDN D11 D10 D09 D08 D07 D06 D05 D04 D03 D02 D01 D00
	uint8_t buf_tx[2] =
	   { uint8_t( ( ( index & 0x1 ) << 7 ) | ( ( gain & 1 ) << 5 ) | ( ( shutdown & 1 ) << 4 ) | ( ( value_12bit >> 8 ) & 0x0f )  ),
	     uint8_t( value_12bit & 0xff ) };
	bool b_res = spi_write( &dev->spi_device, buf_tx, sizeof ( buf_tx ) );
	if ( !b_res )
	{
		return ret;
	}

	ret = true;

	return ret;
}
bool
mcp4822_spi_output_value_double_precision( MCP4822_SPI * dev, uint32_t value_24bit )
{
	// TODO(theGiallo, 2019-12-19): IMPLEMENT
	(void)dev;
	(void)value_24bit;
	log_err( "TODO(theGiallo, 2019-12-19): IMPLEMENT" );
	return false;
}
bool
mcp4822_spi_output_value_double_precision( MCP4822_SPI * dev, float voltage_value )
{
	uint8_t gain = voltage_value > MCP4822_SPI::OUT_MAX_V ? 2 : 1;
	uint32_t value_24bit = mcp4822_spi_encode_volt_24bit( voltage_value, gain );
	bool ret = mcp4822_spi_output_value_double_precision( dev, value_24bit );
	return ret;
}
inline
float
mcp4822_spi_decode_volt( uint16_t value_12bit, uint8_t gain )
{
	tg_assert( gain == 1 || gain == 2 );
	float reference_voltage = MCP4822_SPI::OUT_MAX_V * gain;
	float ret = float( value_12bit ) * reference_voltage / float( 0x0fff );
	return ret;
}
inline
uint16_t
mcp4822_spi_encode_volt( float voltage, uint8_t gain )
{
	tg_assert( voltage >= 0 );
	tg_assert( gain == 1 || gain == 2 );

	float reference_voltage = MCP4822_SPI::OUT_MAX_V * gain;
	voltage = voltage > 0 ? voltage : 0;
	voltage = voltage <= reference_voltage ? voltage : reference_voltage;

	#if MCP3202_DOUBLE_PRECISION
	uint16_t ret = uint16_t( double( 0x0fff ) * ( voltage / double( reference_voltage ) ) );
	#else
	uint16_t ret = uint16_t( float( 0x0fff ) * ( voltage / reference_voltage ) );
	#endif
	return ret;
}
inline
uint32_t
mcp4822_spi_encode_volt_24bit( float voltage, uint8_t gain )
{
	tg_assert( voltage >= 0 );
	tg_assert( gain == 1 || gain == 2 );

	float reference_voltage = MCP4822_SPI::OUT_MAX_V * gain;
	voltage = voltage > 0 ? voltage : 0;
	voltage = voltage <= reference_voltage ? voltage : reference_voltage;

	uint16_t ret = uint16_t( float( 0xffffff ) * ( voltage / reference_voltage ) );
	return ret;
}
void
mcp4822_init( MCP4822_SPI * mcp4822_dev, uint8_t LDAC_pin )
{
	zero_struct( *mcp4822_dev );
	mcp4822_dev->spi_device.mode                   = 0;
	mcp4822_dev->spi_device.bits_per_word          = 8;
	mcp4822_dev->spi_device.bytes_MOSI_MISO_switch = 2;
	mcp4822_dev->spi_device.is_3_wire              = 1;
	mcp4822_dev->spi_device.speed_hz               = 20000000;

	mcp4822_dev->LDAC_pin                 = LDAC_pin;
	if ( tggpio_is_pin_valid( mcp4822_dev->LDAC_pin ) )
	{
		#if 0
		gpioSetMode( mcp4822_dev->LDAC_pin, PI_OUTPUT );
		#else
		TGGPIO_INP( mcp4822_dev->LDAC_pin );
		TGGPIO_OUT( mcp4822_dev->LDAC_pin );
		#endif

		TGGPIO_SETI( mcp4822_dev->LDAC_pin );
	}
}

#if MCP4822_SPI_TEST
int
mcp4822_test( int argc, char *argv[] )
{
	(void)( argc );
	(void)( argv );

	int ret = 0;

	const int OUT_PINS_COUNT = 2;
	const int VALUES_COUNT = 64000 * 3;
	const int TOTAL_VALUES = VALUES_COUNT * OUT_PINS_COUNT;
	const f32 VOLTAGE_MAX = MCP4822_SPI::OUT_MAX_V;

	spi_init( SPI_Device::Lib::PIGPIO );
	tggpio_setup();

	MCP4822_SPI mcp4822_dev;
	mcp4822_init( &mcp4822_dev, 0xFF );

	//mcp4822_dev.spi_device.mode          = SPI_MODE_1;

	mcp4822_dev.spi_device.SPI_number    = 0;
	mcp4822_dev.spi_device.SPI_channel   = 1;

	u64 start_ns, end_ns, tot_ns, tot_ns_sleeped;
	f64 ns_ps, Hz;
	#if 1
	u64 usec_sleep = 1e6f / 44100.0f; // 22.6757
	#else
	u64 usec_sleep = 1000f;
	#endif

	float   voltage   = 0;
	uint8_t mcp_index = 0;
	bool b_res;
	b_res = spi_open( &mcp4822_dev.spi_device );
	if ( !b_res )
	{
		ret = 1;
		goto mcp4822_test_exit_cannot_open;
	}

	start_ns = nano_now();
	for ( int i = 0; i != VALUES_COUNT; ++i )
	{
		f32 t = i / f32(VALUES_COUNT);
		f32 p = 0.5f + 0.5f * sin( t * 6.283f * 4.0f );
		voltage = p * VOLTAGE_MAX;
		for ( mcp_index = 0; mcp_index != OUT_PINS_COUNT; ++mcp_index )
		{
			#if 1
			b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, voltage * ( mcp_index + 1 ) );
			#else
			u8 gain = mcp_index;
			u16 v = u16(i & 0x0fff);
			//v = 0x0fff;
			b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, v, gain, 0 );
			//b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, v, gain, 1 );
			#endif
			if ( ! b_res )
			{
				fprintf( stderr, "error outputting voltage from pin %u\n", (uint32_t)mcp_index );
				ret = 1;
				goto mcp4822_test_exit_close;
			}
		}
		usleep_busy( usec_sleep );
	}
	end_ns = nano_now();
	tot_ns = end_ns - start_ns;
	ns_ps = tot_ns / f64(TOTAL_VALUES);
	Hz = TOTAL_VALUES / ( f64(tot_ns) / 1e9 );
	tot_ns_sleeped = usec_sleep * u64(VALUES_COUNT * 1000);
	fprintf( stderr, "sleeping %lluus per sample (%lluns in total), %d samples took %lluns, %fns per sample, %fHz\n",
	                 usec_sleep, tot_ns_sleeped, TOTAL_VALUES, tot_ns, ns_ps, Hz );

	if ( tot_ns_sleeped > tot_ns )
	{
		log_err( "tot_ns_sleeped > tot_ns" );
	}

	tot_ns -= tot_ns_sleeped;
	ns_ps = tot_ns / (f64)(TOTAL_VALUES);
	Hz = TOTAL_VALUES / ( f64(tot_ns) / 1e9 );
	fprintf( stderr, "%d samples took %lluns, %fns per sample, %fHz\n", TOTAL_VALUES, tot_ns, ns_ps, Hz );

	for ( mcp_index = 0; mcp_index != OUT_PINS_COUNT; ++mcp_index )
	{
		#if 0
		b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, voltage * ( mcp_index + 1 ) );
		#else
		u8 gain = 1;
		u16 v = 0;
		b_res = mcp4822_spi_output_value( &mcp4822_dev, mcp_index, v, gain, 1 );
		#endif
		if ( ! b_res )
		{
			fprintf( stderr, "error outputting voltage from pin %u\n", (uint32_t)mcp_index );
			ret = 1;
			goto mcp4822_test_exit_close;
		}
	}

	mcp4822_test_exit_close:
	b_res = spi_close( &mcp4822_dev.spi_device );
	mcp4822_test_exit_cannot_open:

	spi_exit( SPI_Device::Lib::PIGPIO );

	return ret;
}
#endif
