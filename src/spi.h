#ifndef _SPI_H_
#define _SPI_H_ 1

#include <stdio.h>
#include <unistd.h>
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>

#include "macro_tools.h"

struct
SPI_Device
{
	enum class
	Lib : uint8_t
	{
		PIGPIO,
		KERNEL_DRIVER,
	};
	// NOTE(theGiallo): spi_devs[HW device][channel]
	// device 0 has 2 channels
	// device 1 has 3 channels
	static const char * spi_devs[2][3];
	struct
	GPIO_Pins
	{
		uint8_t CE0_GPIO_pin;
		uint8_t CE1_GPIO_pin;
		uint8_t CE2_GPIO_pin;
		uint8_t MOSI_GPIO_pin;
		uint8_t MISO_GPIO_pin;
		uint8_t SCLK_GPIO_pin;
	} static gpio_pins[2];
	struct
	{
		uint32_t mode                   : 2;
		uint32_t active_high_CE0        : 1;
		uint32_t active_high_CE1        : 1;
		uint32_t active_high_CE2        : 1;
		uint32_t not_reserved_spi_CE0   : 1;
		uint32_t not_reserved_spi_CE1   : 1;
		uint32_t not_reserved_spi_CE2   : 1;
		uint32_t SPI_number             : 1;
		uint32_t is_3_wire              : 1;
		uint32_t bytes_MOSI_MISO_switch : 4;
		uint32_t ls_bit_MOSI_first      : 1;
		uint32_t ls_bit_MISO_first      : 1;
		uint32_t bits_per_word          : 6;
		uint32_t SPI_channel            : 2;
	};
	uint32_t speed_hz;
	int      fd;
	Lib      lib;
};

bool
spi_init( SPI_Device::Lib lib );
void
spi_exit( SPI_Device::Lib lib );
bool
spi_open( SPI_Device * spi_dev );
bool
spi_close( SPI_Device * spi_dev );
bool
spi_read( SPI_Device * spi_dev, uint8_t * buf_rx, int length );
bool
spi_write( SPI_Device * spi_dev, uint8_t * buf_tx, int length );
bool
spi_write_read( SPI_Device * spi_dev, uint8_t * buf, int length );
bool
spi_write_read( SPI_Device * spi_dev, uint8_t * buf_tx, uint8_t * buf_rx, int length );

#endif /* ifndef _SPI_H_  */
