#ifndef __MCP3202_H__
#define __MCP3202_H__ 1

#include "spi.h"

// NOTE(theGiallo): freq max is 0.9MHz with VDD=2.7V and 1.8 with VDD=5V
// NOTE(theGiallo): freq min is 10kHz
// NOTE(theGiallo): SPI mode can be 0 or 3
// NOTE(theGiallo): CS must be high before and after acquisition,
// pigpio sets the CS pin to active and to inactive, before and after comm.
// At spiOpen it set it to active and then inactive.
struct
MCP3202_SPI
{
	SPI_Device spi_device;
	float      reference_voltage;
};

bool
mcp3202_spi_read_value( MCP3202_SPI * dev, uint8_t index, uint16_t * out_value );
bool
mcp3202_spi_read_diff_value( MCP3202_SPI * dev, uint8_t index_positive, uint16_t * out_value );
bool
mcp3202_spi_read_value( MCP3202_SPI * dev, uint8_t sgl1_diff0, uint8_t index, uint16_t * out_value );
inline
float
mcp3202_spi_sample_to_volt( MCP3202_SPI * dev, uint16_t sample );
bool
mcp3202_spi_read_value( MCP3202_SPI * dev, uint8_t sgl1_diff0, uint8_t index, float * voltage );
bool
mcp3202_spi_read_value( MCP3202_SPI * dev, uint8_t index, float * voltage );
bool
mcp3202_spi_read_diff_value( MCP3202_SPI * dev, uint8_t index_positive, float * voltage );
void
mcp3202_init( MCP3202_SPI * mcp3202_dev );

#if MCP3202_SPI_TEST
int
mcp3202_test( int argc, char * argv[] );
#endif

#endif /* ifndef __MCP3202_H__ */
