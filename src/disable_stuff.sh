 #!/bin/bash

sudo systemctl stop systemd-journald.socket
sudo systemctl stop systemd-journald-dev-log.socket
sudo systemctl stop cron rpi-eeprom-update systemd-timesyncd systemd-journald

# The Pi is going to use simple uni-cast DNS - multi-cast DNS support is not required.
sudo systemctl stop avahi-daemon.socket
sudo systemctl stop avahi-daemon

sudo systemctl stop bluetooth

## Stop the ntp service
sudo systemctl stop ntp

## Stop the triggerhappy service
sudo systemctl stop triggerhappy.socket
sudo systemctl stop triggerhappy

## Stop the dbus service. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
sudo systemctl stop dbus.socket
sudo systemctl stop dbus

## Stop the console-kit-daemon service. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
sudo killall console-kit-daemon

## Stop the polkitd service. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
sudo killall polkitd

## Only needed when Jack2 is compiled with D-Bus support (Jack2 in the AutoStatic RPi audio repo is compiled without D-Bus support)
#export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/dbus/system_bus_socket

## Remount /dev/shm to prevent memory allocation errors
#sudo mount -o remount,size=128M /dev/shm

## Kill the usespace gnome virtual filesystem daemon. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
killall gvfsd

## Kill the userspace D-Bus daemon. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
killall dbus-daemon

## Kill the userspace dbus-launch daemon. Warning: this can cause unpredictable behaviour when running a desktop environment on the RPi
killall dbus-launch

## Uncomment if you'd like to disable the network adapter completely
#echo -n “1-1.1:1.0” | sudo tee /sys/bus/usb/drivers/smsc95xx/unbind
## In case the above line doesn't work try the following
#echo -n “1-1.1” | sudo tee /sys/bus/usb/drivers/usb/unbind

## Set the CPU scaling governor to performance
#echo -n performance | sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

## And finally start JACK
#jackd -P70 -p16 -t2000 -d alsa -dhw:UA25 -p 128 -n 3 -r 44100 -s &

#exit

#Also, as long as you don't need networking, adding these also frees up some resources:
#sudo systemctl ifplugd stop
#sudo killall ifplugd
#sudo systemctl networking stop
