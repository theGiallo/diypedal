#ifndef __TGGPIO_H__
#define __TGGPIO_H__ 1

#include "tgtime.h"

// NOTE(theGiallo): from https://elinux.org/RPi_GPIO_Code_Samples#Direct_register_access

#define TGGPIO_BCM2708_PERI_BASE        0x20000000
#define TGGPIO_GPIO_BASE                (TGGPIO_BCM2708_PERI_BASE + 0x200000) /* GPIO controller */
#define TGGPIO_PAGE_SIZE (4*1024)
#define TGGPIO_BLOCK_SIZE (4*1024)

int  tggpio_mem_fd;
void * tggpio_map;

// I/O access
volatile unsigned *tggpio;


// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define TGGPIO_INP(g) *(tggpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define TGGPIO_OUT(g) *(tggpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define TGGPIO_SET_ALT(g,a) *(tggpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))

#define TGGPIO_SET *(tggpio+7)  // sets   bits which are 1 ignores bits which are 0
#define TGGPIO_CLR *(tggpio+10) // clears bits which are 1 ignores bits which are 0
#define TGGPIO_SETI(g) TGGPIO_SET = 1 << ( g ) // sets   bits which are 1 ignores bits which are 0
#define TGGPIO_CLRI(g) TGGPIO_CLR = 1 << ( g ) // clears bits which are 1 ignores bits which are 0

#define TGGPIO_GET(g) (*(tggpio+13)&(1<<g)) // 0 if LOW, (1<<g) if HIGH

#define TGGPIO_PULL *(tggpio+37) // Pull up/pull down
#define TGGPIO_PULLCLK0 *(tggpio+38) // Pull up/pull down clock

enum class tggpio_Pull: u8
{
	OFF = 0,
	PULL_DOWN = 1,
	PULL_UP = 2,
};
inline
void
tggpio_set_pull( int pin, tggpio_Pull pull )
{
	TGGPIO_PULL = u32( pull );
	cksleep_busy( CPU_CLOCK_FROM_NS( 1 ) );
	TGGPIO_PULLCLK0 = 1 << pin;
	cksleep_busy( CPU_CLOCK_FROM_NS( 1 ) );
	TGGPIO_PULL = u32( 0 );
	TGGPIO_PULLCLK0 = u32( 0 );
}

inline
bool
tggpio_is_pin_valid( int pin )
{
	bool ret = pin >= 0 && pin <= 27;
	/*
	BCM GPIO
	 2  3
	 3  5
	 4  7
	17  11
	27  13
	22  15
	10  19
	 9  21
	11  23
	 0  27
	 5  29
	 6  31
	13  33
	19  35
	26  37
	14  8
	15  10
	18  12
	23  16
	24  18
	25  22
	 8  24
	 7  26
	 1  28
	12  32
	16  36
	20  38
	21  40
	*/
	return ret;
}

bool
tggpio_setup();

struct
tggpio_Button
{
	enum class Status : u8
	{
		UP,
		DOWN
	};
	enum class Front : u8
	{
		STATIC,
		UP,
		DOWN
	};

	u32 start_ck_down;
	u32 last_ck_down;
	u32 start_ck_up;
	u32 last_ck_up;
	Status status;
	Front  front;
	u8  bcm_gpio_pin;
};
void
tggpio_button_setup( tggpio_Button * button, u8 bcm_gpio_pin );
void
tggpio_button_read( tggpio_Button * button );

#if TGGPIO_EXAMPLE
void
tggpio_example_print_button( int g );
int
tggpio_example( int argc, char ** argv );
#endif

#endif /* ifndef __TGGPIO_H__ */
