#if 0
	USER=pi
	REMOTE=192.168.1.3
	#REMOTE=rpi00
	REMOTE_PATH=/home/pi/
	TMUX_TARGET=0
	REL_ROOT_PROJ=../

	THIS_FILE_PATH=$0
	THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	#THIS_FILE_DIR=${THIS_FILE_PATH%/*}
	THIS_FILE=${THIS_FILE_PATH##*/}
	EXECUTABLE_NAME=${THIS_FILE%%.*}

	#echo this file dir = "'$THIS_FILE_DIR'"
	#echo this file path = "'$THIS_FILE_PATH'"
	#echo this file = "'$THIS_FILE'"
	#echo executable name = "'$EXECUTABLE_NAME'"

	if [ -z "${CXX+x}" ]
	then
		CXX=arm-linux-gnueabihf-g++
	fi

	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]=DEBUG=1

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/include/"

	declare -a LIBS_DIRS
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$REL_ROOT_PROJ"libraries/pigpio/usr/local/lib/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	#LIBS[${#LIBS[@]}]=pigpiod_if2
	LIBS[${#LIBS[@]}]=pigpio
	LIBS[${#LIBS[@]}]=rt

	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}
	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}

	ret=3

	echo "calling compiler"

	$CXX -std=c++11 -Wall -Wextra -Wno-comment `include_dirs` `libs_dirs` `link_libs` `defines` -O3 -fPIC -o "$EXECUTABLE_NAME" "$THIS_FILE";
	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		printf 'will copy file to remote and run it\n'
		printf "user = '$USER'\n"
		printf "remote = '$REMOTE'\n"

		scp -i ../bin/pi_id_rsa "./$EXECUTABLE_NAME" $USER@$REMOTE:$REMOTE_PATH \
		&& \
		ssh -i ../bin/pi_id_rsa $USER@$REMOTE "tmux send-keys -t $TMUX_TARGET sudo\ $REMOTE_PATH$EXECUTABLE_NAME \"$@\" Enter"

		ret=$?

		if [ ! $ret -eq 0 ]
		then
			printf "scp failed\n"
		fi
		trash "$EXECUTABLE_NAME"
		#trash "$EXECUTABLE_NAME.o"
	else
		printf 'compilation failed\n' >&2;
	fi


	echo "exiting $ret"
	exit $ret
#endif

#include <stdlib.h>
//#include <pigpiod_if2.h>
#include <pigpio.h>
#include "mcp3008.h"
#include "basic_types.h"
#include "macro_tools.h"
#include "tggpio.h"
#include "tgtime.h"

struct
Multiplexer
{
	uint8_t gpio_pins_le[3]; // NOTE(theGiallo): little endian
	int8_t  gpio_pin_inh; // NOTE(theGiallo): inhibit, if is up, all channels are closed

	//int pi; // NOTE(theGiallo): as returned by pigpio_start
};

void
multiplexer_setup( Multiplexer * multiplexer )
{
	//int res;
	int gpio_pins_count = 3;
	for ( int i = 0; i != gpio_pins_count; ++i )
	{
		//res = set_mode( multiplexer->pi, multiplexer->gpio_pins_le[i], PI_OUTPUT );
		//IF_ERR_LOG_ERR_AND_EXIT( res, multiplexer_setup_exit );
		int pin = multiplexer->gpio_pins_le[i];
		TGGPIO_INP( pin );
		TGGPIO_OUT( pin );
	}
	//multiplexer_setup_exit:
	return;
}

void
multiplexer_select( Multiplexer * multiplexer, int index )
{
	int mask   = 0x7;
	int masked = mask & index;
	//int pi = multiplexer->pi;
	int gpio_pins_count = 3;
	//int res;
	for ( int i = 0; i != gpio_pins_count; ++i )
	{
		//res = gpio_write( pi, multiplexer->gpio_pins_le[i], 1 & ( masked >> i ) );
		//IF_ERR_LOG_ERR_AND_EXIT( res, multiplexer_select_exit );
		int pin = multiplexer->gpio_pins_le[i];
		if ( 1 & ( masked >> i ) )
		{
			TGGPIO_SETI( pin );
		} else
		{
			TGGPIO_CLRI( pin );
		}
	}
	//multiplexer_select_exit:
	return;
}

#if 0
// NOTE(theGiallo): this is not working, investigate here:
// https://www.raspberrypi.org/forums/viewtopic.php?f=63&t=155830
static inline unsigned int get_cyclecount (void)
{
	unsigned int value;
	// Read CCNT Register
	asm volatile ("MRC p15, 0, %0, c9, c13, 0\t\n": "=r"(value));
	return value;
}

static inline void init_perfcounters (int32_t do_reset, int32_t enable_divider)
{
	// in general enable all counters (including cycle counter)
	int32_t value = 1;

	// peform reset:
	if (do_reset)
	{
		value |= 2;     // reset all counters to zero.
		value |= 4;     // reset cycle counter to zero.
 	}

	if (enable_divider)
		value |= 8;     // enable "by 64" divider for CCNT.

	value |= 16;

	// program the performance-counter control-register:
	asm volatile ("MCR p15, 0, %0, c9, c12, 0\t\n" :: "r"(value));

	// enable all counters:
	asm volatile ("MCR p15, 0, %0, c9, c12, 1\t\n" :: "r"(0x8000000f));

	// clear overflows:
	asm volatile ("MCR p15, 0, %0, c9, c12, 3\t\n" :: "r"(0x8000000f));
}

static
inline
u32
get_ticks()
{
	u32 cc;
	static s32 init = 0;
	if ( ! init )
	{
		log_dbg( "init" );
		#if 0
		__asm__ __volatile__ ( "mcr p15, 0, %0, c9, c12, 2" :: "r" ( 1 << 31 ) ); // stop the cc
		__asm__ __volatile__ ( "mcr p15, 0, %0, c9, c12, 0" :: "r" ( 5 ) );       // initialize
		__asm__ __volatile__ ( "mcr p15, 0, %0, c9, c12, 1" :: "r" ( 1 << 31 ) ); // start the cc
		init = 1;
		#else

		/* enable user-mode access to the performance counter */
		asm volatile("mcr p15, 0, %0, c9, c14, 0" :: "r"(1));
		/* enable all counters */
		asm volatile("mcr p15, 0, %0, c9, c12, 1" :: "r"(0x8000000f));

		init_perfcounters( 0, 0 );
		#endif
		log_dbg( "init done" );
	}
	__asm__ __volatile__ ( "MRC p15, 0, %0, c9, c13, 0" : "=r" ( cc ) );
	return cc;
}
#endif

int
main( int argc, char *argv[] )
{
	(void)( argc );
	(void)( argv );

	int ret = 0;

	float   voltage   = 0;
	uint8_t mcp_index = 0;
	bool b_res;

	FILE* prof;

	int multiplexer_indices[] = {1, 2};
	int m_count = ARRAY_SIZE(multiplexer_indices);

	u64 sampling_start, sampling_end;
	u64 ns;
	double ns_ps;
	double Hz;
	//u32 sampling_start_cc, sampling_end_cc;
	//u32 ticks;
	//double ticks_ps;

	int samples_count = 8000;

	{
		int res = gpioInitialise();
		if ( res < 0 )
		{
			return 1;
		}
	}

	if ( ! tggpio_setup() )
	{
		log_err( "failed to setup tggpio" );
		ret = 1;
		return ret;
	}

	MCP3008_SPI mcp3008_devs[m_count];
	for ( int i = 0; i != m_count; ++i )
	{
		zero_struct( mcp3008_devs[i] );
		mcp3008_devs[i].spi_device.lib           = SPI_Device::Lib::PIGPIO;
		mcp3008_devs[i].spi_device.active_high_CE0 = 0;
		mcp3008_devs[i].spi_device.SPI_number    = 0;
		mcp3008_devs[i].spi_device.SPI_channel   = 0;
		mcp3008_devs[i].spi_device.mode          = SPI_MODE_0;
		mcp3008_devs[i].spi_device.speed_hz      = 3.5e6;//1.35e6;//44100;
		mcp3008_devs[i].spi_device.bits_per_word = 8;
		//mcp3008_devs[i].reference_voltage        = 3.3f;
		//mcp3008_devs[i].reference_voltage        = 5.0f;
		mcp3008_devs[i].reference_voltage        = 1.0f;
	}

	//int pi = pigpio_start( 0, 0 );
	//if ( pi < 0 )
	//{
	//    ret = 1;
	//    goto multiplex_mcp3008_test_exit_cannot_open_pi;
	//}

	Multiplexer multiplexer;
	zero_struct( multiplexer );
	                        // BCM // GPIO ID
	multiplexer.gpio_pins_le[0] = 17; // #11
	multiplexer.gpio_pins_le[1] = 27; // #13
	multiplexer.gpio_pins_le[2] = 22; // #15
	//multiplexer.pi = pi;

	b_res = spi_open( &mcp3008_devs[0].spi_device );

	if ( !b_res )
	{
		ret = 1;
		goto multiplex_mcp3008_test_exit_cannot_open_spi;
	}

	for ( int i = 1; i != m_count; ++i )
	{
		mcp3008_devs[i].spi_device = mcp3008_devs[0].spi_device;
	}

	prof = fopen( "./prof.txt", "w" );

	multiplexer_select( &multiplexer, multiplexer_indices[0] );
	sampling_start    = nano_now();
	//sampling_start_cc = get_ticks();
	for ( int i = 0; i != samples_count; ++i )
	{
		for ( int m = 0; m != m_count; ++m )
		{
			u64 start, end;
			(void)start;
			(void)end;
			#if 1
			start = nano_now();
			//u32 start = get_ticks();
			multiplexer_select( &multiplexer, multiplexer_indices[m] );
			end = nano_now();
			//u32 end = get_ticks();
			fprintf( prof, "multiplexer_select %llu nS\n", end - start );
			//fprintf( prof, "multiplexer_select %u nS\n", end - start );
			#else
			if ( m ) continue;
			#endif

			voltage = 0;
			mcp_index = 0;

			//start = get_ticks();

			start = nano_now();
			b_res = mcp3008_spi_read_value( &mcp3008_devs[m], mcp_index, &voltage );
			end = nano_now();
			fprintf( prof, "mcp3008_spi_read_value %llu nS\n", end - start );

			//end = get_ticks();
			//fprintf( prof, "mcp3008_spi_read_value %u nS\n", end - start );

			if ( b_res )
			{
				//log_dbg( "pin %d read voltage = %fV", mcp_index, voltage );
				printf( "%f%s", voltage, m == m_count - 1 ? "\n" : ", " );
			} else
			{
				log_err( "error reading voltage from pin %u (sample %d)", (uint32_t)mcp_index, i );
				ret = 1;
				goto multiplex_mcp3008_test_exit_close;
			}
		}
		//usleep( 1000 );
	}
	sampling_end    = nano_now();
	//sampling_end_cc = get_ticks();
	ns = sampling_end - sampling_start;
	//ticks = sampling_end_cc - sampling_start_cc;
	ns_ps = ns / (double)samples_count;
	//ticks_ps = ticks / (double)samples_count;
	Hz = samples_count / ((double)ns/1e9);
	log_info( "sampling took %llu nS; %f nS per sample (%f Hz)", ns, ns_ps, Hz );
	//log_info( "sampling took %u ticks; %f ticks per sample (%f Hz)", ticks, ticks_ps, Hz );

	fclose( prof );

	multiplex_mcp3008_test_exit_close:
	b_res = spi_close( &mcp3008_devs[0].spi_device );
	multiplex_mcp3008_test_exit_cannot_open_spi:
	//pigpio_stop( pi );
	//multiplex_mcp3008_test_exit_cannot_open_pi:
	return ret;
}
#include "spi.cpp"
#include "mcp3008.cpp"
#include "tggpio.cpp"
