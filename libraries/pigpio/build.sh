#!/bin/bash

THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd repo
tmp=Makefile_$RANDOM
echo "tmp=$tmp"
cp Makefile $tmp
sed -i -e "s/CROSS_PREFIX =.*/CROSS_PREFIX = arm-linux-gnueabihf-/" Makefile
make
DESTDIR=$THIS_FILE_DIR make install
mv $tmp Makefile
